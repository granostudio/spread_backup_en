<?php
/*
 Plugin Name: Grano Banner
 Plugin URI: http://www.granostudio.com.br
 Description: Banner
 Author: Grano Studio
 Version: 1.0.0
 Author URI: http://www.granostudio.com.br
 */
 /*--------------------------------------------------------------
  *			Register Grano Plugin
  *-------------------------------------------------------------*/

	add_action( 'wp_loaded', 'ativarPlugin' );
	function ativarPlugin(){
		$ativar = new GranoPluginsAtivos();
		$ativar->ativar('Banner', 'Banner');

	}

/*--------------------------------------------------------------
 *			Register Slider Post Type
 *-------------------------------------------------------------*/
function cpt_banner()
{
	$labels = array(
		'name'                  => _x( 'Banner', 'Post type general name', 'granostudio' ),
		'singular_name'         => _x( 'Banner', 'Post type singular name', 'granostudio' ),
		'menu_name'             => __( 'Banner', 'granostudio' ),
		'name_admin_bar'        => __( 'Add New on Toolbar', 'granostudio' ),
		'parent_item_colon'   	=> __( 'Banner anterior:', 'granostudio' ),
		'all_items'           	=> __( 'Todos os Banner', 'granostudio' ),
		'view_item'           	=> __( 'Ver fotos do Banner', 'granostudio' ),
		'add_new_item'          => __( 'Add New Banner', 'granostudio' ),
		'add_new'             	=> __( 'Novo Banner', 'granostudio' ),
		'edit_item'           	=> __( 'Editar Grupo de Banner', 'granostudio' ),
		'update_item'         	=> __( 'Update Banner', 'granostudio' ),
		'search_items'        	=> __( 'Procurar Banner', 'granostudio' ),
		'not_found'           	=> __( 'Nenhum banner encontrado', 'granostudio' ),
		'not_found_in_trash'  	=> __( 'Nenhum banner encontrado na lixeira', 'granostudio' )
		);

	$args = array(
		'labels'             	=> $labels,
		'public'             	=> true,
		'publicly_queryable' 	=> true,
		'show_ui'            	=> true,
		'show_in_menu'       	=> true,
		'query_var'          	=> true,
		'rewrite' 						=> true,
		'capability_type'    	=> 'post',
		'show_in_admin_bar'   => true,
		'menu_icon' => 'dashicons-images-alt2',
		'can_export'          => true,
		'has_archive'        	=> true,
		'hierarchical'       	=> false,
		'menu_position'      	=> null,
		'supports'           	=> array( 'title' )
		);

	register_post_type('banner',$args);

}

add_action('init','cpt_banner');

// galeria de imagens para portfolio
add_action( 'cmb2_init', 'cmb2_banner' );
/**
 * Define the metabox and field configurations.
 */
function cmb2_banner() {

		// Start with an underscore to hide fields from custom fields list
		$prefix = 'banner_';

		/**
		 * Initiate the metabox
		 */
		$cmb = new_cmb2_box( array(
				'id'            => $prefix.'banner',
				'title'         => __( 'Slides', 'cmb2' ),
				'object_types'  => array( 'banner', ), // Post type
				'context'       => 'normal',
				'priority'      => 'high',
				// 'show_names'    => true, // Show field names on the left
				// 'cmb_styles' => false, // false to disable the CMB stylesheet
				// 'closed'     => true, // Keep the metabox closed by default
		) );

		$group_slides = $cmb->add_field( array(
				'id'          => $prefix.'slides',
				'type'        => 'group',
				// 'description' => __( 'Generates reusable form entries', 'cmb2' ),
				// 'repeatable'  => false, // use false if you want non-repeatable group
				'options'     => array(
						'group_title'   => __( 'Slide {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
						'add_button'    => __( 'Add Slide', 'cmb2' ),
						'remove_button' => __( 'Remove Slide', 'cmb2' ),
						'sortable'      => true, // beta
						// 'closed'     => true, // true to have the groups closed by default
				),
		) );
		$cmb->add_group_field( $group_slides, array(
				'name' => 'Titulo',
				'id'   => $prefix.'titulo',
				'type' => 'text',
				// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
		) );
		$cmb->add_group_field( $group_slides, array(
				'name' => 'Texto',
				// 'description' => 'Write a short description for this entry',
				'id'   => $prefix.'descricao',
				'type' => 'textarea_small',
		) );
		$cmb->add_group_field( $group_slides, array(
				'name' => 'Imagem',
				'id'   => $prefix.'imagem',
				'type' => 'file',
				'options' => array(
						'url' => false, // Hide the text input for the url
				),
		) );
		$cmb->add_group_field( $group_slides, array(
			'name'        => 'Link com conteúdo do site',
			'id'          => $prefix.'link',
			'type'        => 'post_search_text', // This field type
			// post type also as array
			'post_type'   => array('post','page','clientes'),
			// Default is 'checkbox', used in the modal view to select the post type
			'select_type' => 'radio',
			// Will replace any selection with selection from modal. Default is 'add'
			'select_behavior' => 'replace',
		) );

}
/*--------------------------------------------------------------
 *					Starter Slider
 *-------------------------------------------------------------*/

add_shortcode('nossacasa_slider','nossacasa_slider_shortcode');

function nossacasa_slider_shortcode( $atts, $content = null )
{
	$output = '';
	$slides = get_posts(array( 'post_type' => 'Foto Escola', 'posts_per_page' => 7, 'orderby' => 'menu_order', 'order' => 'DESC' ));

	if(count($slides))
	{
		$output .= '<div id="slider">';
		$output .= '<div id="carousel-nossacasa" class="carousel slide">';
		$output .= '<div class="carousel-inner">';

		foreach ($slides as $key => $post)
		{
			setup_postdata($post);

			$output .= '<div class="item '.(($key == 0)?"active":"").'">';
			//$output .= '<div class="hatch"></div>';
			//if (has_post_thumbnail($post->ID)) {
			//	$output .= get_the_post_thumbnail($post->ID,'full',array('class'=>'img-responsive'));
			//}
			// if ( has_post_thumbnail($post->ID) ) {
			//	 $thumbnail = get_the_post_thumbnail($post->ID,'full',array('class'=>'img-responsive'));
			//}
			if ( has_post_thumbnail($post->ID) ) {
				 $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', array('class'=>'img-responsive' ) );
			}
			$output .= '<div class="foto" style="background-image: url('.$thumbnail[0].');"></div>';
			$output .= '<div class="carousel-caption">';
			$output .= '<div>';
			//$output .= '<h2>'.get_the_title( $post->ID ).'</h2>';
			$output .= '<h2>'.get_the_content().'</h2>';

			$btn_link = get_post_meta( $post->ID,'thm_btn_link',true );
			$btn_text = get_post_meta( $post->ID,'thm_btn_text',true );
			$btn_two_link = get_post_meta( $post->ID,'thm_btn_two_link',true );
			$btn_two_text = get_post_meta( $post->ID,'thm_btn_two_text',true );

			if( !empty( $btn_text ) ){
				$output .= '<a class="btn btn-success btn-lg" href="'.$btn_link.'">'.$btn_text.'</a>  &nbsp; &nbsp;';
			}
			if( !empty( $btn_two_text ) ){
				$output .= '<a class="btn btn-default btn-lg" href="'.$btn_two_link.'">'.$btn_two_text.'</a>';
			}

			$output .= '</div>';
			$output .= '</div>';
			$output .= '</div>';
		}

		wp_reset_postdata();

		$output .= '</div>';
		$output .= '<a class="left carousel-control" href="#carousel-nossacasa" data-slide="prev">';
		$output .= '<i class="fa fa-angle-left"></i>';
		$output .= '</a>';
		$output .= '<a class="right carousel-control" href="#carousel-nossacasa" data-slide="next">';
		$output .= '<i class="fa fa-angle-right"></i>';
		$output .= '</a>';
		$output .= '</div>';
		$output .= '</div>';
	}
	return $output;
}


/*--------------------------------------------------------------
 *					Add Submenu
 *-------------------------------------------------------------*/

function cpt_banner_posts_sort()
{
    add_submenu_page('edit.php?post_type=banner', 'Sort Slide', 'Sort', 'edit_posts', basename(__FILE__), 'cpt_banner_posts_sort_callback');
}

add_action('admin_menu' , 'cpt_banner_posts_sort');


function cpt_banner_posts_sort_callback()
{
	$slides = new WP_Query('post_type=banner&posts_per_page=-1&orderby=menu_order&order=ASC');
?>
	<div class="wrap">
		<h3>Sort Slides<img src="<?php echo home_url(); ?>/wp-admin/images/loading.gif" id="loading-animation" /></h3>
		<ul id="nossacasa-slide-list">
			<?php if($slides->have_posts()): ?>
				<?php while ( $slides->have_posts() ){ $slides->the_post(); ?>
					<li id="<?php the_id(); ?>"><?php the_title(); ?></li>
				<?php } ?>
			<?php else: ?>
				<li>There is no Slide Created</li>
			<?php endif; ?>
		</ul>
	</div>
<?php
}


/*--------------------------------------------------------------
 *				Add Sub-Menu Admin Style
 *-------------------------------------------------------------*/

function nossacasa_slider_posts_sort_styles()
{
	$screen = get_current_screen();

	if($screen->post_type == 'slider')
	{
		wp_enqueue_style( 'sort-stylesheet', plugins_url( '/css/slider-nossacasa-stylesheet.css' , __FILE__ ), array(), false, false );
		wp_enqueue_script('jquery-ui-sortable');
		wp_enqueue_script( 'sort-script', plugins_url( '/js/slider-nossacasa-script.js' , __FILE__ ), array(), false, true );
	}
}

add_action( 'admin_print_styles', 'nossacasa_slider_posts_sort_styles' );


/*--------------------------------------------------------------
 *				Ajax Call-back
 *-------------------------------------------------------------*/

function nossacasa_slider_posts_sort_order()
{
	global $wpdb; // WordPress database class

	$order = explode(',', $_POST['order']);
	$counter = 0;

	foreach ($order as $slide_id) {
		$wpdb->update($wpdb->posts, array( 'menu_order' => $counter ), array( 'ID' => $slide_id) );
		$counter++;
	}
	die(1);
}

add_action('wp_ajax_slide_sort', 'nossacasa_slider_posts_sort_order');
