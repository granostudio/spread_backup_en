<?php
/**
 * Grano Site Classes Uteis
 *
 * @author Grano studio
 */


 /**
  * Verificar Grano Plugins Ativos
  */
  if(!get_option('grano-plugin-ativo')){
    add_option('grano-plugin-ativo', '','','yes');
  }
 class GranoPluginsAtivos
 {

   public $ativos = '';

   function __construct()
   {

   }

   public function ativar($pluginType, $pluginNome){
     if(get_option('grano-plugin-ativo')){
       $ativo = get_option('grano-plugin-ativo');
     }

       $ativo = array(
         "type" => $pluginNome
       );

       update_option('grano-plugin-ativo', json_encode($ativo));
   }
 }
 $ativar = new GranoPluginsAtivos();

 /**
  * Verifica se quais tipos de conteudo foram ativados
  * obs: ele retorna como uma array
  */

class modelosCheck
{
  public function ativos(){
    $custom_post_type = get_option('custom_post_type');
    $ativos = $custom_post_type['posttype_opt'];
    return $ativos;
  }
}


/**
 * Função para pegar theme options
 */

function grano_get_options($page,$campo){
   $valor = get_option($page);
   return $valor[$campo];
}
