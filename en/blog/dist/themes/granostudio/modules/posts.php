<?php

/**
* Módulo:
* ***** Posts - Page Template *****
*
* @package WordPress
* @subpackage Grano Studio
* @since Grano Studio 1.0
 */

function module_posts(){
    ?>
    <div class="container grano-posts">
      <div class="row">
        <?php
           $args = array( 'post_type' => 'post', 'posts_per_page' => 10 );
        	 $loop = new WP_Query( $args );
        	while ( $loop->have_posts() ) :
            $loop->the_post();
        	  ?>
            <div class="col-sm-4 post">
              <?php if (has_post_thumbnail()){?>
                <?php echo get_the_post_thumbnail($loop->get_the_ID(), 'medium', array( 'class' => 'img-responsive' ) ); ?>
              <?php } else { ?>
                <img src="<?php echo get_template_directory_uri(); ?>/img/default/carousel.gif" class="img-responsive" />
              <?php }; ?>
              <h2><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></h2>
              <p>
                <?php the_excerpt(); ?>
              </p>
              <a class="btn-default btn" href="<?php echo get_the_permalink(); ?>">Leia mais</a>
            </div>
            <?php
        	endwhile; ?>
      </div>
    </div>

    <?
}
 ?>
