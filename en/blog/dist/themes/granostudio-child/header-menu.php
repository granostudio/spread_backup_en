<!-- NAV BAR ============================================================== -->

<header id="masthead" class="site-header" role="banner">
		<div id="sticky_nav">

			<div id="cabecalho_topo">
				<div class="row">

					<div class="col-xs-7 col-sm-7">
						<nav id="cabecalho_breadcrumb">
							<nav id="breadcrumb_nav" class="breadcrumb">
	<ul class="breadcrumb_list">
					<li class="breadcrumb_item rotulo"><h3 class="breadcrumb_titulo">You are here:</h3></li>
			<li class="breadcrumb_item home atual"><a href='https://spread.com.br/en/blg' class="breadcrumb_link">Blog</a></li>	</ul><!-- .breadcrumb_list -->
</nav><!-- .breadcrumb -->						</nav>
					</div><!-- .col -->

					<div class="col-xs-5 col-sm-5">
						<nav id="cabecalho_linguas">
							<h3 class="linguas_rotulo">language</h3>
							<?php
							if (is_single()){
								$id = get_the_ID();
								$caminhoPT = '/pt-br/blog/?post='.$id;
								$caminhoEN = '/en/blog/?post='.$id;
							}else{
								$caminhoPT = str_replace('en/','pt-br/', $_SERVER['REQUEST_URI']);
								$caminhoEN = str_replace('pt-br/','en/', $_SERVER['REQUEST_URI']);
							}

								?>
								<a href="https://spread.com.br<?= $caminhoPT ?>">Português <img src="//spread.com.br/en/site/wp-content/themes/ms/imagens/brazil.png" alt=""></a>

								<a href="https://spread.com.br<?= $caminhoEN ?>">Inglês <img src="//spread.com.br/en/site/wp-content/themes/ms/imagens/united-states.png" alt=""></a>
														</nav><!-- #cabecalho_linguas -->
					</div><!-- .col -->

				</div><!-- .row -->
			</div><!-- #cabecalho_topo -->
			<div id="menu-hamburguer">
				<span></span>
				<span></span>
				<span></span>
				<span></span>
			</div>
			<div class="menu-mobile">
				<div class="hover-fechar"></div>

				<div class="menu-menu-principal-container"><ul id="menu-menu-principal" class="menu"><li id="menu-item-286" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-2 current_page_item menu-item-286"><a href="https://spread.com.br/en/site/">Home</a></li>
<li id="menu-item-950" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-950"><a href="#">Solutions</a>
<ul class="sub-menu">
	<li id="menu-item-280" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-280"><a href="https://spread.com.br/en/site/solucoes/digital-transformation/">Digital Transformation</a></li>
	<li id="menu-item-283" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-283"><a href="https://spread.com.br/en/site/solucoes/digital-studio/">Digital Studio</a></li>
	<li id="menu-item-334" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-334"><a href="https://spread.com.br/en/site/solucoes/sap-studio/">SAP Studio</a></li>
	<li id="menu-item-285" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-285"><a href="https://spread.com.br/en/site/core-studio/">Core Studio</a></li>
	<li id="menu-item-282" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-282"><a href="https://spread.com.br/en/site/solucoes/devops-quality/">DevOps &#038; Quality</a></li>
	<li id="menu-item-335" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-335"><a href="https://spread.com.br/en/site/solucoes/managed-services/">Managed Services</a></li>
	<li id="menu-item-281" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-281"><a href="https://spread.com.br/en/site/solucoes/business-outsourcing/">Business Outsourcing</a></li>
</ul>
</li>
<li id="menu-item-276" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-276"><a href="https://spread.com.br/en/site/quem-somos/">About Us</a></li>
<li id="menu-item-949" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-949"><a href="https://www.genovahub.com.br/">Genova</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-743"><a href="https://spread.com.br/en/blog/">Blog</a></li>
<li id="menu-item-743" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-743"><a href="https://spread.selecty.com.br/">Work with Us</a></li>
<li id="menu-item-272" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-272"><a href="https://spread.com.br/en/site/contato/">Contact</a></li>
</ul></div>				<div class="col">
					<a href=""" class="btn btn-primary btn-newsletter">Subscribe to our newsletter</a>
					<div class="menu-redes-sociais-container"><ul id="menu-redes-sociais" class="menu_social"><li id="menu-item-351" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-351"><a target="_blank" href="https://www.facebook.com/spreadtecnologia">Facebook</a></li>
<li id="menu-item-352" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-352"><a target="_blank" href="https://twitter.com/spreadoficial">Twitter</a></li>
<li id="menu-item-353" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-353"><a target="_blank" href="https://www.linkedin.com/company/spread">LinkedIn</a></li>
<li id="menu-item-354" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-354"><a target="_blank" href="https://www.instagram.com/spreadtecnologia/">Instagram</a></li>
</ul></div>				</div>

			</div>
			<div id="cabecalho_principal" class="vert_ancestral">

				<div id="cabecalho_logo" class="vert_item">
					<a href="https://spread.com.br/en/site" rel="home">
						<img class="cabecalho_logo_img" src="https://spread.com.br/en/site/wp-content/uploads/2017/12/cabecalho_logo_spread.png" width="123" height="75" alt="Spread" />
					</a>

				</div><!-- #cabecalho_logo -->




				<div id="cabecalho_direita">

					<nav id="menu_principal" class="vert_item">
						<div class="collapse navbar-collapse">
							<div class="menu-menu-principal-container"><ul id="menu-menu-principal-1" class="menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-2 current_page_item menu-item-286"><a href="https://spread.com.br/en/site/">Home</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-950"><a href="#">Solutions</a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-280"><a href="https://spread.com.br/en/site/solucoes/digital-transformation/">Digital Transformation</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-283"><a href="https://spread.com.br/en/site/solucoes/digital-studio/">Digital Studio</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-334"><a href="https://spread.com.br/en/site/solucoes/sap-studio/">SAP Studio</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-285"><a href="https://spread.com.br/en/site/core-studio/">Core Studio</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-282"><a href="https://spread.com.br/en/site/solucoes/devops-quality/">DevOps &#038; Quality</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-335"><a href="https://spread.com.br/en/site/solucoes/managed-services/">Managed Services</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-281"><a href="https://spread.com.br/en/site/solucoes/business-outsourcing/">Business Outsourcing</a></li>
</ul>
</li>
<li id="menu-item-276" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-276"><a href="https://spread.com.br/en/site/quem-somos/">About Us</a></li>
<li id="menu-item-949" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-949"><a href="https://www.genovahub.com.br/">Genova</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-743"><a href="https://spread.com.br/en/blog/">Blog</a></li>
<li id="menu-item-743" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-743"><a href="https://spread.selecty.com.br/">Work with Us</a></li>
<li id="menu-item-272" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-272"><a href="https://spread.com.br/en/site/contato/">Contact</a></li>
</ul></div>						</div><!-- .collapse -->

					</nav><!-- #menu_principal -->

					<div id="cabecalho_newsletter" class="vert_item"><a href="" class="btn btn-primary btn-newsletter">Subscribe to our newsletter</a></div>



					<nav id="cabecalho_redes" class="vert_item">
						<div class="menu-redes-sociais-container"><ul id="menu-redes-sociais-1" class="menu_social"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-351"><a target="_blank" href="https://www.facebook.com/spreadtecnologia">Facebook</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-352"><a target="_blank" href="https://twitter.com/spreadoficial">Twitter</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-353"><a target="_blank" href="https://www.linkedin.com/company/spread">LinkedIn</a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-354"><a target="_blank" href="https://www.instagram.com/spreadtecnologia/">Instagram</a></li>
</ul></div>					</nav><!-- #cabecalho_redes -->

				</div><!-- #cabecalho_direita -->

			</div><!-- #cabecalho_principal -->

		</div><!-- #sticky_nav -->

		<!-- newsletter -->
		<div class="news-box">
			<div class="fechar">
				<span></span>
				<span></span>
				<span></span>
				<span></span>
			</div>
			<h1>Sprint Spread - Subscribe to our newsletter</h1>
			<div id="newsletter-site-0000ba31a03c53d2e794"></div>
			<script type="text/javascript" src="https://d335luupugsy2.cloudfront.net/js/rdstation-forms/stable/rdstation-forms.min.js"></script>
			<script type="text/javascript">
			  new RDStationForms('newsletter-site-0000ba31a03c53d2e794-html', '').createForm();
			</script>
			<style>
				#conversion-newsletter-site-0000ba31a03c53d2e794{
					background-color:transparent!important;
				}
				#conversion-modal-newsletter-site-0000ba31a03c53d2e794 .modal-content section form input, #conversion-modal-newsletter-site-0000ba31a03c53d2e794 .modal-content section form select, #conversion-modal-newsletter-site-0000ba31a03c53d2e794 .modal-content section form textarea, #conversion-modal-newsletter-site-0000ba31a03c53d2e794 .modal-content section form .select2-choice, #conversion-newsletter-site-0000ba31a03c53d2e794 section input, #conversion-newsletter-site-0000ba31a03c53d2e794 section select, #conversion-newsletter-site-0000ba31a03c53d2e794 section textarea, #conversion-newsletter-site-0000ba31a03c53d2e794 section .select2-choice {
				    display: inline-block !important;
				    background-color: #FFFFFF !important;
				    vertical-align: middle !important;
				    font-size: 18px !important;
				    line-height: 20px !important;
				    width: 100% !important;
				    height: 50px!important;
				    margin: 0 !important;
				    padding: 20px !important;
				    -webkit-border-radius: 3px !important;
				    -moz-border-radius: 3px !important;
				    border-radius: 25px !important;
				}
				#conversion-newsletter-site-0000ba31a03c53d2e794 section div.actions .call_button{
					cursor: pointer !important;
			    height: auto !important;
			    text-align: center !important;
			    text-decoration: none !important;
			    font-weight: bold !important;
			    font-size: 20px !important;
			    word-break: break-word !important;
			    line-height: 1.2em !important;
			    white-space: normal !important;
			    vertical-align: middle !important;
			    margin: 2px auto 24px auto !important;
			    padding: 15px 20px 17px 20px !important;
			    -webkit-border-radius: 3px !important;
			    -moz-border-radius: 3px !important;
			    border-radius: 25px !important;
				}
				#conversion-newsletter-site-0000ba31a03c53d2e794 #conversion-form-newsletter-site-0000ba31a03c53d2e794 p.notice{
					text-align: center!important;
					color:#fff!important;
				}
			</style>
					</div>

	</header><!-- #masthead -->
<!-- /NAV BAR ============================================================== -->
