<?php
session_start();
$token = "d41d8cd98f00b204e9800998ecf8427e";
$senhaAcesso = 'granostudio';

if($_REQUEST['token'] == $token)
    $tokenValido = true;

if($_REQUEST['senhaAcesso'] == $senhaAcesso && empty($_SESSION['usuarioValido']))
    $_SESSION['usuarioValido'] = true;

if($_REQUEST['sair'])
    unset($_SESSION['usuarioValido']);
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
<meta charset="utf-8"/>
<title>Usando GIT para atualizar arquivos no servidor de hospedagem</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<pre>
    <?php
        if($tokenValido) {
            if($_SERVER['REQUEST_URI']=="/dev/deploy/index.php?token=d41d8cd98f00b204e9800998ecf8427e"){
              $exec = shell_exec("git pull origin staging 2>&1");
              echo "dev ";
            } else {
              $exec = shell_exec("git pull origin production 2>&1");
              echo "prod ";
            }


            echo $exec;

            $textoLog = PHP_EOL."Data: ".date(d."/".m."/".Y." - ".H.":".i.":".s);
            $textoLog .= PHP_EOL.$exec;


            if($_SERVER['REQUEST_URI']=="dev/index.php"){
              $arquivoLog = fopen('log-dev.txt', 'a+');
              fwrite($arquivoLog, $textoLog);
              fclose($arquivoLog);
            } else {
              $arquivoLog = fopen('log.txt', 'a+');
              fwrite($arquivoLog, $textoLog);
              fclose($arquivoLog);
            }

        } else if($_SESSION['usuarioValido']) {
        ?>
        <form action="index.php" method="post">
            <input type="hidden" name="token" value="d41d8cd98f00b204e9800998ecf8427e">
            <input type="submit" value="Atualizar">
        </form>
        <?php
        if($_SESSION['usuarioValido'])
            echo '<p><a href="index.php?sair=true">Sair</a></p>';
            $texto = file('log.txt');
            foreach ($texto as $linha) {
                echo $linha;
            }
        } else {
        ?>
        <form action="index.php" method="post">
            <div>
                <input type="text" placeholder="Senha" name="senhaAcesso">
            </div>
            <input type="submit" value="Acessar Sistema">
        </form>
        <?php
        }
    ?>
</pre>
</body>
</html>
