<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */


 $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
 $urlParts = explode('/', str_ireplace(array('http://', 'https://'), '', $url));
 $subdom = array_shift((explode('.', $_SERVER['HTTP_HOST'])));
 // Include local configuration
 // if ($subdom == "localhost:8000" || $subdom == "dev" || $urlParts[1] == "dev") {
 // 	include(dirname(__FILE__) . '/local-config.php');
 // }

// Global DB config
if (!defined('DB_NAME')) {
	define('DB_NAME', 'blog_us');
}
if (!defined('DB_USER')) {
	define('DB_USER', 'root');
}
if (!defined('DB_PASSWORD')) {
	define('DB_PASSWORD', 'q1w2e3*2018');
}
if (!defined('DB_HOST')) {
	define('DB_HOST', 'localhost');
}

/** Database Charset to use in creating database tables. */
if (!defined('DB_CHARSET')) {
	define('DB_CHARSET', 'utf8');
}

/** The Database Collate type. Don't change this if in doubt. */
if (!defined('DB_COLLATE')) {
	define('DB_COLLATE', '');
}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
 define('AUTH_KEY',         '+D;)|4E}-7*#vxH`S22zhy)R-FT|bo|+&Bm!;6[U6~-=KDbAKUOd-J!o(yB+9eJM');
 define('SECURE_AUTH_KEY',  '(r;Vsu8dSS[EMJn|ioQ+lC:9hS%2AMYl+{f_4LH-Nv3161#6+LU(w_L4Rq#g3L v');
 define('LOGGED_IN_KEY',    'C&=@p1x;}_+-Iso]+H+k|3-$Tq&` WKpk2Hk!=<(SS<0/#p3Q$N>C-qo9nnpbj z');
 define('NONCE_KEY',        ',?n|kEtjy!J|_d^1i#2?FCQ5unEr7+1@n-|v_FFe5}j.L4; ]t,J.(cl+Oo[cR+*');
 define('AUTH_SALT',        'Uk7]DlwF~|2Z.F9qRi2H_#!1+Pn*-n6ZE8|s:<2z&ZZ//)U4,zdA/sdL0@c`KzLw');
 define('SECURE_AUTH_SALT', 'Nf;&Eb$|Wr&4RWMn /hq$T1e&nfm<y2]r9@R2j=,wBONvIU6Nwk!K_xX)S7x|dVL');
 define('LOGGED_IN_SALT',   '+a+5Dv..seE/b;B,C.i05fBCR5ZG>+,4M[<{QtZe.lBpJW$oN_:Y`Q/`SA0q.%mQ');
 define('NONCE_SALT',       ']Bg)Pgt@-x7i+QGe|tdMI8IjhGc]iJ5$83_y7/|i.(sbylC}ffIB|O6Y,UOE*PhV');


/**#@-*/
/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'gs_';

/**
 * Set custom paths
 *
 * These are required because wordpress is installed in a subdirectory.
 */
if (!defined('WP_SITEURL')) {
	define('WP_SITEURL', 'http://' . $_SERVER['SERVER_NAME'] . '/' . $urlParts[1] . '/'. $urlParts[2] . '/app');
}
if (!defined('WP_HOME')) {
	define('WP_HOME',    'http://' . $_SERVER['SERVER_NAME'] . '/'. $urlParts[1] . '/'. $urlParts[2] );
}

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
if (!defined('WP_DEBUG')) {
	define('WP_DEBUG', true);
}




/** Mudar o local do wp-content **/
if (!defined('WP_CONTENT_DIR')) {
	define('WP_CONTENT_DIR', dirname(__FILE__) . '/dist');
}
if (!defined('WP_CONTENT_URL')) {
	define('WP_CONTENT_URL', 'http://' . $_SERVER['SERVER_NAME'] . '/'. $urlParts[1] . '/'. $urlParts[2] . '/dist');
}


/** mudar local dos uploads **/
// define( 'UPLOADS', dirname(__FILE__) .'/uploads');

// default theme granoexpresso
if (!defined('WP_DEFAULT_THEME')) {
define( 'WP_DEFAULT_THEME', 'granostudio-child' );
}

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) .'/app');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
