To: contato@spread.com.br
Subject: Novo site WordPress
Date: Tue, 21 Nov 2017 12:29:03 +0000
From: WordPress <wordpress@localhost>
Message-ID: <b7dd0e21ec7b2d4d936b5e5b029c3ad2@localhost>
X-Mailer: PHPMailer 5.2.22 (https://github.com/PHPMailer/PHPMailer)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

Seu novo site WordPress foi configurado com sucesso em:

http://localhost/spread

Você pode fazer login como administrador usando as seguintes informações:

Usuário: admin_spread
Senha: A senha que você escolheu durante a instalação.
Faça login aqui: http://localhost/spread/wp-login.php

Esperamos que você goste do seu novo site. Obrigado!

--A equipe do WordPress
https://wordpress.org/

