To: contatos@cyclesafe.com.br
Subject: Novo site WordPress
Date: Fri, 10 Nov 2017 11:48:50 +0000
From: WordPress <wordpress@localhost>
Message-ID: <c1f5e55b753d629e5b15b1da093cfeef@localhost>
X-Mailer: PHPMailer 5.2.22 (https://github.com/PHPMailer/PHPMailer)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

Seu novo site WordPress foi configurado com sucesso em:

http://localhost/cycle

Você pode fazer login como administrador usando as seguintes informações:

Usuário: admin_cycle
Senha: A senha que você escolheu durante a instalação.
Faça login aqui: http://localhost/cycle/wp-login.php

Esperamos que você goste do seu novo site. Obrigado!

--A equipe do WordPress
https://wordpress.org/

