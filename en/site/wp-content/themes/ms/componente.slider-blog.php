<?php

// @requer imgTema, elemento, el, componente postsBlog, componente owl

// -----------------------------------------------------------------------
// INTRO

elemento( 'h1', el( 'span', $param_titulo, 'linha1' ), 'secao_titulo' );
elemento( 'p', $param_intro, 'secao_intro' );

// -----------------------------------------------------------------------
// SLIDER

$args = array(
	// 'category'				=> CAT_BLOG,
	'orderby'				=> 'date',
	// 'ignore_sticky_posts'	=> $param_ordem != 'sticky',
	'posts_per_page'		=> $param_qtd_posts,
);

/*
if ( $param_ordem != 'sticky' )
	$args['post__not_in'] = $param_sticky_posts;
else
	$args['post__in'] = $param_sticky_posts;
*/

if ( $param_ordem == 'sticky' )
	$args['sticky'] = true;

// $listaPosts = get_posts( $args );
$listaPosts = getPostsBlog( $args );
$totalPosts = count( $listaPosts );

$owlParams = array(
	'id'			=> $param_id,
	'class'			=> $param_class,
	'slides'		=> array(),
	'opcoes'		=> array(
		'loop'				=> true,
		'nav'				=> true,
		'dots'				=> false,
		'autoplay'			=> true,
		'autoplayTimeout'	=> 6000,
		'autoplayHoverPause'=> true,
		'margin'			=> 25,
		'responsive'		=> array(
			0	=> array(
				'items'		=> 1,
			),
			768 => array(
				'items'		=> ceil( $param_colunas / 2 ),
			),
			1024 => array(
				'items'		=> $param_colunas,
			),
		),
	),
);

// -----------------------------------------------------------------------
// SLIDES INDIVIDUAIS

$listaParams = array(
	'class'		=> 'slider_blog_lista',
	'colunas'	=> 1,
);

$totalSlides = ceil( $totalPosts / $param_colunas );

for ( $i = 0; $i < $totalPosts; $i++ ) {
	$slidePosts = array_slice( $listaPosts, $i, 1 );
	$owlParams['slides'][] = Componente::postsBlog( $listaParams + array( 'posts' => $slidePosts ), false );
}

// -----------------------------------------------------------------------
// RENDERIZAÇÃO DO SLIDER

Componente::owl( $owlParams );

// -----------------------------------------------------------------------
// BOTÃO

ancora( PAGINA_BLOG, 'Ir para o blog' . imgTema( 'mais_lilas.png', '&rarr;', 'blog_botao_img' ), 0, 'btn blog_botao' );
