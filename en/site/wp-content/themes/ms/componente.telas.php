<div <?= $this->attr( $this->_attrWrapper ) ?>>
	<div class="telas_wrapper">
		<?php foreach ( $param_telas as $tela ) : ?>
			<div class="tela">
				<?= $tela ?>
			</div><!-- .tela -->
		<?php endforeach; ?>
	</div><!-- .telas_wrapper -->
</div><!-- .telas -->