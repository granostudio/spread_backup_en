<?php
/*
 * @param (string) id
 * @param (string) style
 * @param (string) class
 * @param (array_n) secoes
 * 		@param (string) titulo
 * 		@param (string) conteudo
 * 		@param (string) class
 * 		@param (string) style
 * @param (string) parent
 *
 * @requer attr, obter, idUnico
 */
 
isset( $params ) OR die();
 
$wrapperAttr = array(
	'id'			=> 'acordiao' . idUnico('acordiao'),
	'style'			=> null,
	'class'			=> null,
);

$itemAttr = array(
	'style'			=> null,
);

if ( !isset( $params['secoes'] ) || !is_array( $param_secoes ) )
	return print '[Componente Acordião] Sem seções!';

$id = obter( 'id', $params, $wrapperAttr['id'] );

?>
<div <?= attr( $wrapperAttr, $params ) ?> role="tablist" aria-multiselectable="true">
	<?php
	$n = 0;
	foreach ( $param_secoes as $secao ) :
		$n++;
		$tabId = "$id-tab$n";
		if ( !isset( $secao['titulo'] ) || !isset( $secao['conteudo'] ) )
			continue;
		?>
		<div <?= attr( $itemAttr, $params ) ?> class="panel panel-default <?= obter( 'class', $itemAttr ) ?><?php if ( $n == 1 ) print ' ativo' ?>">
			<div class="panel-heading" role="tab" id="<?= $tabId ?>">
				<h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#<?= obter( 'parent', $params, $id ) ?>" href="#<?= $tabId ?>_conteudo" aria-expanded="true" aria-controls="collapseOne" class="panel-title-anchor"><?= $secao['titulo'] ?></a>
				</h4><!-- .panel-title -->
			</div><!-- .panel-heading -->
			<div id="<?= $tabId ?>_conteudo" class="panel-collapse collapse<?php if ( $n == 1 ) print ' in' ?>" role="tabpanel" aria-labelledby="<?= $tabId ?>">
				<div class="panel-body"><?= $secao['conteudo'] ?></div><!-- .panel-body -->
			</div><!-- .panel-collapse -->
		</div><!-- .panel -->
		<?php
	endforeach;
	?>
</div><!-- #<?= $id ?> -->

<script>
	+function() {
		var $paineis = jQuery('#<?= $id ?>');
		$paineis.find('.collapse')
			.on( 'show.bs.collapse', function() {
				var $this = jQuery(this),
					$painel = $this.parents('.panel');
				$painel.addClass('ativo');
			} )
			.on( 'hide.bs.collapse', function() {
				var $this = jQuery(this),
					$painel = $this.parents('.panel');
				$painel.removeClass('ativo');
			} );
	}();
</script>