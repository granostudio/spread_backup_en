<?php
/*
 * Facilita a criação de elementos HTML com atributos dinâmicos e elementos filhos
 *
 * @requer Wordpress
 */

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// CLASSE BASE

class Element {
	
	public $callParent = true;
	
	public $tag;
	public $content;
	public $tree;
	public $separator;
	public $classify;
	
	private $_parent;
	private $_generation;
	private $_attr = array();
	private $_children = array();
	
	function __construct( $tag, $content = null, $attr = null, $parent = null, $generation = 0 ) {
		// Seta propriedades
		$this->tag = strtolower( $tag );
		$this->content = $content;
		$this->_attr = E::sanitizeAttr( $attr );
		$this->_parent = $parent;
		$this->_generation = $generation;
		// Copia opções da fábrica
		$this->classify = E::$classify;
		$this->tree = E::$tree;
		$this->separator = E::$separator;
	}
	
	// -----------------------------------------------------------------------------
	// Métodos mágicos
	
	/*
	 * Renderiza este elemento e os filhos
	 */
	function __toString() {
		if ( $this->callParent && $this->_parent )
			return (string) $this->_parent;
		if ( !E::$renderEmpty && !in_array( $this->tag, E::$selfClosing ) && !$this->_children && ( is_null( $this->content ) || $this->content === '' ) )
			return '';
		if ( $this->_children ) {
			$childHtml = array();
			$this->content = '';
			$i = 0;
			foreach ( $this->_children as $child ) {
				$i++;
				$child->callParent = false;
				if ( $this->classify )
					$child->addClass( 'item' . $i );
				$childHtml[] = (string) $child;
				$child->callParent = true;
			}
			switch ( $this->separator ) {
				case E::SEPARATOR_SPACE :
					$this->content .= implode( ' ', $childHtml );
				break;
				case E::SEPARATOR_NL :
					$this->content .= implode( "\n", $childHtml );
				break;
				case E::SEPARATOR_INDENT :
					$this->content .= "\n";
					$this->content .= implode( "\n", $childHtml );
					$this->content .= "\n";
					if ( $this->_generation )			
						$this->content .= str_repeat( "\t", $this->_generation );
				break;
				case E::SEPARATOR_BR :
					$this->content .= implode( '<br />', $childHtml );
				break;
				case E::SEPARATOR_NONE :
					$this->content .= implode( '', $childHtml );
				break;
				default :
					$this->content .= implode( $this->separator, $childHtml );
			}
		}
		if ( $this->classify )
			$this->addClass( 'gen' . ( $this->_generation + 1 ) );
		$html = $this->_renderElement();
		if ( $this->separator === E::SEPARATOR_INDENT && $this->_generation )			
			$html = str_repeat( "\t", $this->_generation ) . $html;
		return $html;
	}
	
	/*
	 * Instancia e anexa um descendente
	 */
	function __call( $func, $args ) {
		return $this->child( $func, isset( $args[0] ) ? $args[0] : null, isset( $args[1] ) ? $args[1] : null );
	}
	
	/*
	 * Retorna o valor de um atributo
	 */
	function __get( $prop ) {
		return isset( $this->_attr[ $prop ] )
			? $this->_attr[ $prop ]
			: null
		;
	}
	
	/*
	 * Define o valor de um atributo
	 */
	function __set( $prop, $val ) {
		$this->_attr[ $prop ] = $val;
		return $val;
	}
	
	// -----------------------------------------------------------------------------
	// Métodos públicos
	
	/*
	 * Registra um filho deste elemento
	 */
	function child( $tag, $content = null, $attr = null, $treeMode = null ) {
		$novoIndice = count( $this->_children );
		$this->_children[] = new Element( $tag, $content, $attr, $this, $this->_generation + 1 );
		if ( is_null( $treeMode ) )
			$treeMode = $this->tree;
		switch ( $treeMode ) {
			case E::TREE_AUTO :
				return !in_array( $tag, E::$selfClosing ) && ( is_null( $content ) || $content === false )
					? $this->_children[ $novoIndice ]
					: $this
				;
			case E::TREE_VERTICAL :
				return $this->_children[ $novoIndice ];
			case E::TREE_HORIZONTAL :
			default :
				return $this;
		}
	}
	
	/*
	 * Registra um nódulo de texto como filho
	 */
	function text( $content, $treeMode = null ) {
		return $this->child( '', $content, array(), $treeMode );
	}
	
	function a( $href, $label = '', $target = false, $attr = array() ) {
		// Facilita a criação de uma âncora
		// @requer esc_url, site_url, get_the_permalink
		$attr = E::sanitizeAttr( $attr );
		if ( $href ) {
			if ( is_int( $href ) )
				$href = get_the_permalink( $href );
			$href = esc_url( $href );
			if ( $target === E::AUTO ) {
				// Modo automático - aciona para links externos
				$target = preg_match( '#^https?://#', $href ) && !preg_match( '#^'.site_url().'#', $href );
			}
			if ( $target )
				$attr['target'] = '_blank';
		} elseif ( !E::$renderEmpty ) {
			return false;
		}
		$attr['href'] = $href;
		return $this->child(
			'a',
			$label,
			$attr
		);
	}
	
	/*
	 * Retorna o elemento que criou este
	 */
	function parent() {
		return $this->_parent;
	}
	
	/*
	 * Retorna um dos filhos
	 */
	function eq( $n = 0 ) {
		if ( !isset( $this->_children[$n] ) )
			return null;
		return $this->_children[$n];
	}
	
	/*
	 * Retorna o último filho
	 */
	function last() {
		if ( !$this->_children )
			return null;
		$last = count( $this->_children ) - 1;
		return $this->_children[$last];
	}
	
	/*
	 * Altera o valor de um atributo e retorna o objeto para chaining
	 */
	function attr( $a, $v ) {
		$this->_attr[ $a ] = $v;
		return $this;
	}
	
	/*
	 * Altera o valor de uma propriedade pública ou uma opção, e retorna o objeto para chaining
	 */
	function prop( $p, $v ) {
		if ( substr( $p, 0, 1 ) == '_' )
			throw new Exception('Propriedade "'.$p.'" não pode ser escrita no elemento "'.$this->tag.'"!');
		$this->$p = $v;
		return $this;
	}
	
	/*
	 * Adiciona uma ou mais classes separadas por espaços ao atributo do objeto
	 */
	function addClass( $cl ) {
		if ( !isset( $this->_attr['class'] ) )
			$this->_attr['class'] = '';
		$new = explode( ' ', $cl );
		$cur = $this->_attr['class']
			? explode( ' ', $this->_attr['class'] )
			: array()
		;
		foreach ( $new as $c ) {
			if ( in_array( $c, $cur ) )
				continue;
			$cur[] = $c;
		}
		$this->_attr['class'] = implode( ' ', $cur );
		return $this;
	}
	
	/*
	 * Retorna a representação HTML do objeto
	 */
	function html() {
		return (string) $this;
	}
	
	/*
	 * Prefixa os atributos com 'data-'
	 * Se $ignoreCommonAttr estiver ativado, ignora atributos na lista E::$commonAttr
	 */
	function prefixData( $ignoreCommonAttr = true ) {
		$newAttr = array();
		foreach ( $this->_attr as $a => $v ) {
			if ( $ignoreCommonAttr && in_array( $a, E::$commonAttr ) ) {
				$newAttr[$a] = $v;
				continue;
			}
			if ( preg_match( '/^data-/', $a ) ) {
				$newAttr[$a] = $v;
				continue;
			}
			$newAttr[ 'data-' . $this->_hifenize($a) ] = is_bool( $v )
				? ( $v ? 'true' : 'false' )
				: $v
			;
		}
		$this->_attr = $newAttr;
		return $this;
	}
	
	// Métodos privados
	
	/*
	 * Insere hífens em lugar de espaços e underlines, e também antes de maiúsculas; converte para minúsculas
	 */
	private function _hifenize( $a ) {
		if ( !preg_match( '/[A-Z_ ]/', $a ) )
			return $a;
		$a = preg_replace_callback( '/[A-Z]/', function($matches) { return '-' . strtolower( $matches[0] ); }, $a );
		$a = preg_replace( '#[ _]+#', '-', $a );
		$a = preg_replace( '#-{2,}#', '-', $a );
		$a = preg_replace( '#^-|-$#', '', $a );
		return $a;
	}
	
	/*
	 * Gera o HTML dos atributos
	 */
	private function _renderAttr() {
		if ( !$this->_attr )
			return '';
		$html = array();
		foreach ( $this->_attr as $a => $v ) {
			if ( is_null( $v ) ) {
				continue;
			} elseif ( is_string( $v ) ) {
				$html[] = $a . '="' . esc_attr( trim( $v ) ) . '"';
			} elseif ( is_bool( $v ) ) {
				if ( $v )
					$html[] = $a;
			} elseif ( is_array( $v ) ) {
				$html[] = $a . '="' . json_encode( $v ) . '"';
			}
		}
		return implode( ' ', $html );
	}
	
	/*
	 * Gera o HTML desse elemento, utilizando o conteúdo da variável interna $content
	 */
	private function _renderElement() {
		$html = '';
		if ( $this->tag ) {
			// Opening tag
			$html .= '<' . $this->tag;
			if ( $this->_attr )
				// Attributes
				$html .= ' ' . $this->_renderAttr();
			// End opening tag
			if ( is_scalar( $this->content ) )
				$html .= '>';
		}
		if ( is_scalar( $this->content ) )
			$html .= $this->content;
		if ( $this->tag ) {
			if ( is_scalar( $this->content ) )
				// Closing tag
				$html .= '</' . $this->tag . '>';
			else
				// Self-closing
				$html .= ' />';
		}
		return $html;
	}
	
}



// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// FÁBRICA

class E {
	
	const AUTO = -1;
	
	/*
	 * Ao chamar child() no primeiro elemento, o método retornará:
	 * TREE_HORIZONTAL	: Ele mesmo, para que chamadas seguintes criem irmãos ao filho criado
	 * TREE_VERTICAL	: O filho, para que a chamada seguinte crie um filho do filho
	 * TREE_AUTO		: Se o elemento atual possuir conteúdo, trata-o como o nódulo final, retornando ao pai; se não possuir, trata-o como nódulo intermediário, retornando a si mesmo
	 */
	const TREE_HORIZONTAL = 1;
	const TREE_VERTICAL = 2;
	const TREE_AUTO = self::AUTO;
	static $tree = self::TREE_AUTO;
	
	/*
	 * Utilizado para separar filhos do primeiro elemento ao gerar a string HTML
	 */
	const SEPARATOR_NONE = 0;
	const SEPARATOR_SPACE = 1;
	const SEPARATOR_NL = 2;
	const SEPARATOR_INDENT = 3;
	const SEPARATOR_BR = 4;
	static $separator = self::SEPARATOR_NONE;
	
	const TARGET_AUTO = self::AUTO;
	
	/*
	 * Adiciona classes aos elementos e seus filhos identificando o nível da árvore (genX) e sua sequência (itemX)
	 */
	static $classify = false;
	
	/*
	 * Renderiza elementos sem conteúdo ou sem link
	 */
	static $renderEmpty = false;
	
	/*
	 * Atributos a serem ignorados pelo método prefixData()
	 */
	static $commonAttr = array( 'id', 'class', 'style', 'src', 'href', 'target', 'type', 'title', 'alt', 'value' );
	
	/*
	 * Elementos que não tem filhos
	 */
	static $selfClosing = array( 'input', 'br', 'hr', 'img', 'meta', 'link', 'col' );
	
	/*
	 * Fábrica para criar um elemento qualquer
	 */
	static function __callStatic( $func, $args ) {
		return new Element(
			$func,
			isset( $args[0] ) ? $args[0] : null,
			isset( $args[1] ) ? $args[1] : null
		);
	}
	
	/*
	 * Fábrica alterada para âncoras
	 */
	static function a( $href, $label = '', $target = false, $attr = array() ) {
		// @requer esc_url, site_url, get_the_permalink
		$attr = self::sanitizeAttr( $attr );
		if ( $href ) {
			if ( is_int( $href ) )
				$href = get_the_permalink( $href );
			$href = esc_url( $href );
			if ( $target === self::AUTO ) {
				// Modo automático - aciona para links externos
				$target = preg_match( '#^https?://#', $href ) && !preg_match( '#^'.site_url().'#', $href );
			}
			if ( $target )
				$attr['target'] = '_blank';
		} elseif ( !self::$renderEmpty ) {
			return false;
		}
		$attr['href'] = $href;
		return new Element(
			'a',
			$label,
			$attr
		);
	}
	
	/*
	 * Fábrica alterada para imagem com wrapper
	 */
	static function imagem( $src, $attrWrapper = array(), $alt = null, $tamanho = 'full' ) {
		// Atributos
		$attrWrapper = self::sanitizeAttr( $attrWrapper );
		if ( !isset( $attrWrapper['class'] ) )
			$attrWrapper['class'] = 'imagem';
		$attrImagem = array(
			'class'		=> $attrWrapper['class'] . '_img'
		);
		if ( is_numeric( $src ) ) {
			// Busca nos anexos do WP
			$dadosImagem = wp_get_attachment_image_src( (int) $src, $tamanho );
			if ( !$dadosImagem )
				return false;
			$attrImagem['src'] = $dadosImagem[0];
			$attrImagem['width'] = $dadosImagem[1];
			$attrImagem['height'] = $dadosImagem[2];
			$attrImagem['class'] .= ' tamanho_' . $tamanho;
			if ( !$alt )
				$alt = get_post_meta( (int) $src, '_wp_attachment_image_alt', true );
		} else {
			// Imagem do tema
			$attrImagem['src'] = TEMA . '/imagens/' . $src;
		}
		if ( $alt )
			$attrImagem['alt'] = $alt;
		// Elemento
		return self::div( null, $attrWrapper )
			->img( null, $attrImagem );
	}
	
	/*
	 * Reseta as opções para os seus padrões
	 */
	static function reset() {
		self::$tree = self::TREE_AUTO;
		self::$separator = SEPARATOR_NONE;
		self::$classify = false;
	}
	
	/*
	 * Retorna sempre uma array
	 * Permite atributos como string HTML, e permite classes como string simples
	 */
	static function sanitizeAttr( $attr ) {
		if ( is_array( $attr ) )
			return $attr;
		elseif ( is_string( $attr ) ) {
			if ( substr( $attr, -1 ) == '"' )
				return explode( ' ', $attr );
			else
				return array( 'class' => $attr );
		}
		else
			return array();
	}
	
}
