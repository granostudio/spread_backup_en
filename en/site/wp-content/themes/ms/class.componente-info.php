<?php

class Componente_Info extends Componente_Abstrato {
	
	protected $_attrWrapper = array( 'id', 'class', 'data-anim', 'data-anim-duration', 'data-anim-delay' );
	
	public function __construct( $novosParams = null ) {
		// Configurações do componente
		$this->_nome = 'Info';
		$this->_slug = 'info';
		$this->_imagem = null;
		$this->_aceitarParamsNovos = false;
		$this->_padroes = array(
			'id'				=> null,
			'class'				=> 'info_item',
			'data-anim'			=> null,
			'data-anim-duration'=> null,
			'data-anim-delay'	=> null,
			'icone'				=> null,
			'fa'				=> null,
			'titulo'			=> null,
			'texto'				=> null,
			'alinhar'			=> true,
			'esc_titulo'		=> true,
			'esc_texto'			=> true,
		);
		$this->_sanitizadores = array(
		);
		$this->_obrigatorios = array(
			'titulo'			=> true,
		);
		$this->_validadores = array(
			'id'				=> 'is_string',
			'class'				=> 'is_string',
			'fa'				=> 'is_string',
			'titulo'			=> 'is_string',
			'texto'				=> 'is_string',
		);
		$this->_regrasJuncao = array(
			'class'				=> '+',
		);
		// Inicialização
		parent::__construct( $novosParams );
	}
	
	public function renderizar( $_imprimir = true ) {
		
		// Se o ícone for o ID de uma imagem na biblioteca, obtém o caminho dela
		if ( is_int( $this->icone ) ) {
			$att = wp_get_attachment_image_src( $this->icone, 'medium' );
			$this->icone = $att
				? $att[0]
				: null
			;
		}
		
		// Se animações estiverem presentes, adiciona a classe e unidade nos valores
		if ( $this->_params['data-anim'] ) {
			if ( !preg_match( '/\banim\b/', $this->class ) )
				$this->class .= ' anim';
			if ( is_numeric( $this->_params['data-anim-duration'] ) )
				$this->_params['data-anim-duration'] = (string) $this->_params['data-anim-duration'] . 's';
			if ( is_numeric( $this->_params['data-anim-delay'] ) )
				$this->_params['data-anim-delay'] = (string) $this->_params['data-anim-delay'] . 's';
		}
		
		// Alinhamento vertical dos conteúdos
		if ( $this->alinhar )
			$this->class .= ' vert_ancestral';
		
		return parent::renderizar( $_imprimir );
		
	}
		
}