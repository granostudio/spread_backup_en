<?php
// Insere a paginação em um arquivo de posts
// Sem parâmetros

global $wp_query;

if ( !$wp_query->post_count ) :
	return;
elseif ( $wp_query->max_num_pages <= 1 || $wp_query->get('paged') == $wp_query->max_num_pages ) :
	elemento( 'p', 'Fim da listagem' . '.', cl('sem_paginacao') );
	return;
endif;

?>
<nav class="paginacao">
	<?php
	the_posts_pagination( array(
		'prev_text'				=> 'Página anterior',
		'next_text'				=> 'Próxima página',
		'mid_size'				=> 2,
		//'screen_reader_text'
	) );
	?>
</nav><!-- .paginacao -->