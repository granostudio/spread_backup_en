<?php

class Componente_Posts_Agenda extends Componente_Abstrato {
	
	protected $_attrWrapper = array( 'id', 'class' );
	
	public function __construct( $novosParams = null ) {
		// Configurações do componente
		$this->_nome = 'Posts do Agenda';
		$this->_slug = 'posts_agenda';
		$this->_aceitarParamsNovos = false;
		$this->_padroes = array(
			'id'				=> null,
			'class'				=> 'posts_agenda',
			'posts'				=> null,
			'query'				=> null,
			'post_class'		=> 'post_item',
			'usar_thumb'		=> false,
			'colunas'			=> 1,
			'msg_sem_posts'		=> true,
		);
		$this->_sanitizadores = array(
			'colunas'			=> 'intval',
		);
		$this->_obrigatorios = array(
		);
		$this->_validadores = array(
			'colunas'			=> 'naoVazio',
			'posts'				=> 'is_array',
			'query'				=> 'is_array',
			'id'				=> 'is_string',
			'class'				=> 'is_string',
			'post_class'		=> 'is_string',
		);
		$this->_regrasJuncao = array(
			'class'				=> '+',
			'post_class'		=> '+',
		);
		// Inicialização
		parent::__construct( $novosParams );
	}
	
	public function renderizar( $_imprimir = true ) {
		
		global $wp_query;
		
		// Se presente uma query, roda-a para obter os posts
		if ( $this->query )
			$this->posts = get_posts( $this->query );
		// Por padrão, obtém os posts da query atual
		elseif ( !$this->posts )
			$this->posts = $wp_query->posts;
			
		// Classe de thumb
		
		$this->post_class .= ' ' . ( $this->usar_thumb ? 'com' : 'sem' ) . '_img';
			
		// Classe de equalização
		
		if ( $this->colunas > 1 && strpos( $this->post_class, 'equalizar_item' ) === false )
			$this->post_class .= ' equalizar_item';
				
		return parent::renderizar( $_imprimir );
		
	}
		
}