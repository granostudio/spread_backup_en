<?php
/*
 * @param (string) id
 * @param (string) style
 * @param (string) class
 * @param (array_n) slides
 * 		@param (string) conteudo
 * 		@param (string) class
 * 		@param (string) style
 * @param (array_a) options
 *
 * @requer imprimirJS
 */
?>
<div <?= $this->attr( $this->_attrWrapper, null ) ?>>
	<?php foreach ( $param_slides as $slide ) : ?>
		<div class="owl-slide"><?= $slide ?></div>
	<?php endforeach; ?>
</div><!-- .owl-carousel -->
<script>
	jQuery( function() {
		<?php imprimirJS( 'opcoes', $param_opcoes, true ) ?>
		jQuery("#<?= $param_id ?>").owlCarousel(opcoes);
	} );
</script>