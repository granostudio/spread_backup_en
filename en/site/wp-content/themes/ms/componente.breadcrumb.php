<?php
// Gera uma trilha de links para o destino atual
// @requer fa, elemento, ancora
$tipoHome = get_option('show_on_front');
$classeItem = 'breadcrumb_item';
$classeAncora = array( 'class' => 'breadcrumb_link' );
?>
<nav <?= $this->attr( $this->_attrWrapper ) ?>>
	<ul class="breadcrumb_list">
		<?php
		
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// RÓTULO
		
		if ( $param_rotulo ) :
			?>
			<li class="<?= $classeItem ?> rotulo"><h3 class="breadcrumb_titulo"><?= $param_rotulo ?></h3></li>
			<?php
		endif;
		
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// HOME
		
		if ( $param_incluir_home ) :
			if ( $tipoHome == 'page' && $param_titulo_home_da_pagina )
				$tituloHome = $this->tituloPagina( (int) get_option('page_on_front') );
			elseif ( $param_titulo_home_nome_site )
				$tituloHome = get_option('blogname');
			else
				$tituloHome = $param_titulo_home;
			
			elemento(
				'li',
				ancora(
					site_url(),
					( $param_icone_home ? fa( $param_icone_home . ' home_icone' ) . ' ' : '' ) . $tituloHome,
					0,
					$classeAncora,
					false
				),
				$classeItem . ' home' . ( $tipoHome == 'page' && is_front_page() || $tipoHome == 'posts' && is_home() ? ' atual' : '' )
			);
			
		endif;
		
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// PÁGINA DE POSTS SEPARADA DA HOME
		
		if ( $tipoHome == 'page' && $param_incluir_pagina_posts && ( is_home() || is_single() || is_archive() ) ) :
		
			$homeId = (int) get_option('page_for_posts');
		
			if ( $param_titulo_pp_da_pagina )
				$tituloPP = $this->tituloPagina( $homeId );
			else
				$tituloPP = $param_titulo_pp;
		
			elemento(
				'li',
				ancora( get_the_permalink( $homeId ), $tituloPP, 0, $classeAncora, false ),
				$classeItem . ' pagina_posts' . ( is_home() ? ' atual' : '' )
			);
		
		endif;
		
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// NÃO MOSTRA MAIS NADA SE ESTIVER NA HOME
		
		if ( is_front_page() || is_home() ) : 0;
		
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// POST
		
		elseif ( is_single() ) :
		
			// Categorias
			
			$cats = $this->hierarquiaCategoriasPost();
			
			if ( $cats ) :
				foreach ( $cats as $c ) :
					elemento(
						'li',
						ancora( get_term_link( $c, 'category' ), $c->name, 0, $classeAncora, false ),
						$classeItem . ' categoria nivel' . $c->hierarquia
					);
				endforeach;
			endif;
				
			// Post
			
			elemento(
				'li',
				ancora( get_the_permalink(), get_the_title(), 0, $classeAncora, false ),
				$classeItem . ' post atual'
			);
			
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// PÁGINA
		
		elseif ( is_page() ) :
		
			elemento(
				'li',
				ancora( get_the_permalink(), get_the_title(), 0, $classeAncora, false ),
				$classeItem . ' pagina atual'
			);
			
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// CATEGORIA
		
		elseif ( is_category() ) :
		
			// Obtém os antecessores
		
			$taxonomy = 'category';
			$termoAtual = $wp_query->queried_object;
			$hierarquia = array( $termoAtual );
			
			while ( $termoAtual->parent ) {
				$termoAtual = get_term( $termoAtual->parent, $taxonomy );
				if ( $this->ignorar_cats && !in_array( $termoAtual->term_id, $this->ignorar_cats ) )
					$hierarquia[] = $termoAtual;
			}
			
			$hierarquia = array_reverse( $hierarquia );
			$ultimo = count( $hierarquia ) - 1;
		
			foreach ( $hierarquia as $i => $termo ) :
				elemento(
					'li',
					ancora( get_term_link( $termo->term_id, $taxonomy ), $termo->name, 0, $classeAncora, false ),
					$classeItem . ' categoria ' . ( $i == $ultimo ? 'atual' : 'nivel' . $i )
				);
			endforeach;
			
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// TAG
		
		elseif ( is_tag() ) :
		
			elemento(
				'li',
				ancora( get_term_link( $wp_query->queried_object->term_id, 'post_tag' ), '#' . $wp_query->queried_object->name, 0, $classeAncora, false ),
				$classeItem . ' tag atual'
			);
			
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// BUSCA
		
		elseif ( is_search() ) :
		
			$tituloBusca = sprintf( $param_titulo_busca,
				get_search_query()
			);
		
			elemento(
				'li',
				ancora( '#', $tituloBusca, 0, $classeAncora, false ),
				$classeItem . ' busca atual'
			);
		
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// ARQUIVO DE DATA
		
		elseif ( is_date() ) : 0;
		
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// ARQUIVO DO AUTOR
		
		elseif ( is_author() ) :
		
			$linkAutor = get_author_posts_url( $wp_query->queried_object->ID );
		
			elemento(
				'li',
				ancora( $linkAutor, $wp_query->queried_object->display_name, 0, $classeAncora, false ),
				$classeItem . ' autor atual'
			);
		
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// 404
		
		elseif ( is_404() ) :
		
			elemento(
				'li',
				ancora( '#', $param_titulo_404, 0, $classeAncora, false ),
				$classeItem . ' erro404 atual'
			);
		
		// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		// FIM
		
		endif;
		
		?>
	</ul><!-- .breadcrumb_list -->
</nav><!-- .breadcrumb -->