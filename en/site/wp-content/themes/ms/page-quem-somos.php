<?php
/**
 * Template Name: Quem Somos
 */

global $post;

add_action( 'wp_enqueue_scripts', 'telas_enqueue' );

get_header();
extract( get_fields() );

?>

	<?php /*
	<header class="entry-header" <?php fundo( umDe( $fundo, FUNDO_PADRAO ), 'full' ) ?>>
		<div class="header-caption">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-7 anim" <?php animAttr( 'fadeInBottom', 1.4, 0.4 ) ?>>
						<?=
						E::h1( null, 'entry-title' )
							->span( $titulo1, 'linha1' )
							->span( $titulo2, 'linha2' )
							->span( $titulo3, 'linha3' )
						?>
					</div><!-- .col -->
					<div class="col-xs-12 col-sm-5 col-md-4 anim" <?php animAttr( 'fadeInLeft', 0.4, 2 ) ?>>
						<?=
						E::p( null, 'entry-desc' )
							->span( $desc, 'entry-desc-texto' )
						?>
					</div><!-- .col -->
				</div><!-- .row -->
			</div><!-- .container -->
		</div><!-- .header-caption -->
	</header>
	*/ ?>

<div id="quem-somos">

	<?php

	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// CABEÇALHO
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	extract( $cabecalho[0], EXTR_PREFIX_ALL, 'cab' );

	?>

	<header class="quem-somos-banner" <?php fundo( $cab_fundo, 'full' ) ?>>
		<div class="content">
			<div class="container">

				<?=
				E::h1( null, 'cabecalho_titulo' )
					->span( $cab_titulo1, 'linha1' )
					->span( $cab_titulo2, 'linha2' )
				?>
				<script>
				jQuery(document).ready(function($) {
					var counterUpper = function () {
						$('.count').each(function () {
								$(this).prop('Counter',0).animate({
									Counter: $(this).text()
								}, {
									duration: 4000,
									easing: 'swing',
									step: function (now) {
											$(this).text(Math.ceil(now));
									},
									complete: function() {
										$(this).stop(true,true);
									}
								});
								});

					};
							// Perform counts when the element gets into view
							$('.count').waypoint(counterUpper, { offset: '100%', triggerOnce: true });
						});
				</script>
				<div class="stats">
					<div class="row margem-40">
						<?php
						$classeCols = 'col-12 col-sm-3 numeros-empresa';
						$n = 0;
						foreach ( $cab_stats as $item ) :
							$n++;
							?>
							<div class="<?= $classeCols ?>">
								<div class="stats_item">
									<?=
									E::h2( $item['numero'], 'item_numero count' ) .
									E::p( $item['rotulo'], 'item_rotulo' )
									?>
								</div><!-- .stat -->
							</div><!-- .col -->
							<?php
							row( $n, $classeCols );
						endforeach;
						?>
					</div><!-- .row -->
				</div><!-- .stats -->

			</div><!-- .container -->
		</div><!-- .content -->

		<?php
		ancora(
			'#clientes',
			imgTema( 'seta_turquesa.png', '&darr;', 'home_scroll_img' ),
			false,
			'home_scroll turquesa'
		);
		?>

	    <div class="mask">
	      <svg
	     version="1.1"
	     id="no-aspect-ratio"
	     xmlns="http://www.w3.org/2000/svg"
	     xmlns:xlink="http://www.w3.org/1999/xlink"
	     x="0px"
	     y="0px"
	     height="100%"
	     width="100%"
	     viewBox="0 0 100 100"
	     preserveAspectRatio="none">
	        <polygon fill="white" points="100,100 0,100 0,0 "/>
	      </svg>
	    </div><!-- .mask -->

	</header><!-- .quem-somos-banner -->

	<?php

	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// CLIENTES
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	extract( $clientes[0], EXTR_PREFIX_ALL, 'cli' );

	?>

	<section id="clientes" class="secao container">

		<?=
		E::h2( null, 'titulo clientes_titulo' )
			->span( $cli_titulo1, 'linha1' )
			->span( $cli_titulo2, 'linha2' )
		?>

		<div class="row thumb-clientes margem-30 text-align">
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/accor-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/albert-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/alstom.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/ambev-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/banco-brasil-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/bradesco-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/brasilprev-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/brb-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/caixa-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/camargo-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/cardiff-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/carrefour-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/ccb-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/cea-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/cetip-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/cielo-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/claro-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
<!-- 		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/copacabana-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div> -->
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/cvc-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/cyrela-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/dasa-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/deloitte-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/duratex-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/eldorado-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/ge-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/hughes-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/ibm-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/jbs-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/jsl-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/localiza-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/makro-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/marisa-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/metro-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/natura-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/nestle-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/net-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/oi-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/PDG-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/petrobras-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/qualicorp.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/rossi-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/saint-gobain-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/santander-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/sky-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/suzano-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		  <div class="col-6 col-sm-2 col-md-2 col-lg-2">
			<img src="../wp-content/themes/ms/imagens/vivo-logo.jpg" alt="..." class="img-fluid img-thumbnail">
		  </div>
		</div><!-- .row -->

	</section><!-- #clientes -->

	<?php

	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// EXECUTIVOS
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	extract( $executivos[0], EXTR_PREFIX_ALL, 'exec' );

	?>

	<section id="executivos" class="container">

		<div class="row margem-80">

			<div class="col-12 col-md-6 ajuste-padding">
				<?=
				E::h2( null, 'titulo executivos_titulo' )
					->span( $exec_titulo1, 'linha1' )
					->span( $exec_titulo2, 'linha2' )
				?>
				<div class="divisoria"></div>
				<?=
				E::p( $exec_texto, 'texto executivos_texto' )
				?>
			</div><!-- .col -->

			<div class="col-12 col-sm-12 col-md-6 col-lg-6 img-executivos">
				<?php img( $exec_foto_lateral, 'full', cl('foto_lateral') ) ?>
			</div>

		</div><!-- .row -->

		<div class="executivos_individuos vert_ancestral">
			<div class="row">

				<div class="col-xs-12 col-sm-12 col-md-4">
					<section class="individuos_item">
						<div class="item_imagem vert_container vert_item" style="line-height: 325px;">
							<img src="../wp-content/themes/ms/imagens/exec-Cassius-1.png" class="img-col-3">
						</div>
						<div class="item_conteudo">
							<h4 class="item_nome">Cassius Buda</h4>
							<p class="item_cargo">CEO</p>
							<p class="item_email">cassius@spread.com.br</p>
							<!-- <a class="item_telefone">+55 11 555-555-5555</a> -->
						</div>
					</section>
				</div>

				<div class="col-xs-12 col-sm-6 col-md-4">
					<section class="individuos_item">
						<div class="item_imagem vert_container vert_item" style="line-height: 325px;">
							<img src="../wp-content/themes/ms/imagens/exec-ze-alberto.png" class="img-col-3">
						</div>
						<div class="item_conteudo">
							<h4 class="item_nome">José Alberto</h4>
							<p class="item_cargo">COO</p>
							<p class="item_email">jbouca@spread.com.br</p>
							<!-- <a class="item_telefone">+55 11 555-555-5555</a> -->
						</div>
					</section>
				</div>

				<div class="col-xs-12 col-sm-6 col-md-4">
					<section class="individuos_item">
						<div class="item_imagem vert_container vert_item" style="line-height: 325px;">
							<img src="../wp-content/themes/ms/imagens/exec-marcelo.png" class="img-col-3">
						</div>
						<div class="item_conteudo">
							<h4 class="item_nome">Marcelo Silva</h4>
							<p class="item_cargo">CFO</p>
							<p class="item_email">marcelo.jsilva@spread.com.br</p>
							<!-- <a class="item_telefone">+55 11 555-555-5555</a> -->
						</div>
					</section>
				</div>

<!-- 				<?php
				$classeCols = 'col-xs-6 col-md-4';
				$n = 0;
				foreach ( $exec_individuos as $item ) :
					$n++;
					?>
					<div class="<?= $classeCols ?>">
						<section class="individuos_item">
							<div class="item_imagem vert_container vert_item">
								<?=
								obterImg( $item['foto_estatica'], 'full', cl('foto_estatica') ) .
								obterImg( $item['foto_dinamica'], 'full', cl('foto_dinamica') )
								?>
							</div>
							<div class="item_conteudo">
								<?=
								E::h4( $item['nome'], 'item_nome' ) .
								E::p( $item['cargo'], 'item_cargo' ) .
								formatarEmail( $item['email'], 'item_email' ) .
								E::a( 'tel:' . $item['telefone'], $item['telefone'], 0, 'item_telefone' )
								?>
							</div>
						</section>
					</div>
					<?php
					row( $n, $classeCols );
				endforeach;
				?> -->
			</div><!-- .row -->

			<div class="row segundo-paragrafo">

				<div class="col-xs-12 col-sm-6 col-md-3">
					<section class="individuos_item">
						<div class="item_imagem vert_container vert_item">
							<img src="../wp-content/themes/ms/imagens/exec-renato.png">
						</div>
						<div class="item_conteudo">
							<h4 class="item_nome">Renato Valença</h4>
							<p class="item_cargo">Comercial</p>
							<p class="item_email">rvalenca@spread.com.br</p>
							<!-- <a class="item_telefone">+55 11 555-555-5555</a> -->
						</div>
					</section>
				</div>

				<div class="col-xs-12 col-sm-6 col-md-3">
					<section class="individuos_item">
						<div class="item_imagem vert_container vert_item">
							<img src="../wp-content/themes/ms/imagens/exec-alex.png">
						</div>
						<div class="item_conteudo">
							<h4 class="item_nome">Alex Granjeiro</h4>
							<p class="item_cargo">Empreendedorismo</p>
							<p class="item_email">alex.granjeiro@spread.com.br</p>
							<!-- <a class="item_telefone">+55 11 0000 0000</a> -->
						</div>
					</section>
				</div>

				<div class="col-xs-12 col-sm-6 col-md-3">
					<section class="individuos_item">
						<div class="item_imagem vert_container vert_item">
							<img src="../wp-content/themes/ms/imagens/exec-marcelo-negrini-1.png">
						</div>
						<div class="item_conteudo">
							<h4 class="item_nome">Marcelo Negrini</h4>
							<p class="item_cargo">Digital Transformation</p>
							<p class="item_email">marcelo.negrini@spread.com.br</p>
							<!-- <a class="item_telefone">+55 11 555-555-5555</a> -->
						</div>
					</section>
				</div>

        <div class="col-xs-12 col-sm-6 col-md-3">
					<section class="individuos_item">
						<div class="item_imagem vert_container vert_item">
							<img src="../wp-content/themes/ms/imagens/exec-Alessandro.png">
						</div>
						<div class="item_conteudo">
							<h4 class="item_nome">Alessandro Gushiken</h4>
							<p class="item_cargo">Operações</p>
							<p class="item_email">apassianoto@spread.com.br</p>
							<!-- <a class="item_telefone">+55 11 555-555-5555</a> -->
						</div>
					</section>
				</div>

			</div>

		</div><!-- .executivos_individuos -->

	</section><!-- #executivos -->

	<?php

	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// PARCERIAS
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	extract( $parcerias[0], EXTR_PREFIX_ALL, 'par' );

	?>
	<section id="parcerias" class="container">

		<?=
		E::h2( null, 'titulo parcerias_titulo' )
			->span( $par_titulo1, 'linha1' )
			->span( $par_titulo2, 'linha2' )
		?>

		<div class="row thumb-clientes parcerias-estrategicas margem-30">
		  <div class="col-sm-4 col-md-4 col-lg-3">
			<img src="../wp-content/themes/ms/imagens/alcatel.png" alt="..." class="img-fluid img-thumbnail" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
		  </div>

		  <div class="col-sm-4 col-md-4 col-lg-3">
			<img src="../wp-content/themes/ms/imagens/amazon.png" alt="..." class="img-fluid img-thumbnail" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
		  </div>

		  <div class="col-sm-4 col-md-4 col-lg-3">
			<img src="../wp-content/themes/ms/imagens/amdocs.png" alt="..." class="img-fluid img-thumbnail" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
		  </div>

		  <div class="col-sm-4 col-md-4 col-lg-3">
			<img src="../wp-content/themes/ms/imagens/cisco.png" alt="..." class="img-fluid img-thumbnail" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
		  </div>

		  <div class="col-sm-4 col-md-4 col-lg-3">
			<img src="../wp-content/themes/ms/imagens/google.png" alt="..." class="img-fluid img-thumbnail" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
		  </div>

		  <div class="col-sm-4 col-md-4 col-lg-3">
			<img src="../wp-content/themes/ms/imagens/huawei.png" alt="..." class="img-fluid img-thumbnail" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
		  </div>

		  <div class="col-sm-4 col-md-4 col-lg-3">
			<img src="../wp-content/themes/ms/imagens/ibm.png" alt="..." class="img-fluid img-thumbnail" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
		  </div>

		  <div class="col-sm-4 col-md-4 col-lg-3">
			<img src="../wp-content/themes/ms/imagens/microsoft.png" alt="..." class="img-fluid img-thumbnail" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
		  </div>

		  <div class="col-sm-4 col-md-4 col-lg-3">
			<img src="../wp-content/themes/ms/imagens/oracle.png" alt="..." class="img-fluid img-thumbnail" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
		  </div>

		  <div class="col-sm-4 col-md-4 col-lg-3">
			<img src="../wp-content/themes/ms/imagens/panasonic.png" alt="..." class="img-fluid img-thumbnail" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
		  </div>

		  <div class="col-sm-4 col-md-4 col-lg-3">
			<img src="../wp-content/themes/ms/imagens/salesforce.png" alt="..." class="img-fluid img-thumbnail" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
		  </div>

		  <div class="col-sm-4 col-md-4 col-lg-3">
			<img src="../wp-content/themes/ms/imagens/sap.png" alt="..." class="img-fluid img-thumbnail" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
		  </div>
		</div><!-- .row -->

	</section><!-- #parcerias -->

</div><!-- #quem-somos -->

<?php

get_footer();
