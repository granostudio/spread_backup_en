<?php

global $post;

get_header();

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header <?php if ( get_field('escurecer_fundo') ) print 'escurecer' ?>" <?php fundo( umDe( get_post_thumbnail_id( $post->ID ), FUNDO_PADRAO ), 'full' ) ?>>
		<div class="header-caption">
			<div class="container ancorar">
				<?php
				elementoNaoVazio( 'h1',
					subElemento( 'span', get_the_title(), cl('large') ).
					subElementoNaoVazio( 'small', enfase( get_field('subtitulo') ), cl('small') ),
				cl('entry-title') );
				?>
			</div><!-- .container -->
		</div><!-- .header-caption -->
	</header><!-- .entry-header -->
	
	<div class="container">
		<div class="entry-main">
			<div class="entry-content">
			
				<?php the_content() ?>
				
				<?php if ( $servicos_itens = get_field( 'servicos_itens' ) ) : ?>
					<div id="post_servicos">
						<?php
						$n = 0;
						foreach ( $servicos_itens as $item ) :
							$n++;
							extract( $item, EXTR_PREFIX_ALL, 'item' );
							?>
							<article class="servicos_item anim" <?php animAttr( 'fadeInRight', 0.4, $n * 0.2 ) ?>>
								<div class="row">
									<div class="col-xs-12 col-sm-8">
										<div class="item_conteudo">
											<?php
											elemento( 'h2', $item_nome, cl('item_titulo') );
											elemento( 'p', $item_resumo, cl('item_resumo') );
											elemento( 'div', $item_texto, cl('item_texto') );
											?>
										</div><!-- .item_conteudo -->
									</div><!-- .col -->
									<div class="col-xs-12 col-sm-4">
										<div class="item_imagem" <?php fundo( $item_imagem, 'medium' ) ?>></div>
									</div><!-- .col -->
								</div><!-- .row -->
							</article><!-- .servicos_item -->
							<?php
						endforeach;
						?>
					</div><!-- #post_servicos -->
				<?php endif; ?>
				
				
				<?php if ( $parceiros_itens = get_field( 'clientes_itens' ) ) : ?>
					<div id="post_parceiros">
						<div class="row estreito">
							<?php
							$n = 0;
							$classeCols = 'col-xs-6 col-sm-2';
							foreach ( $parceiros_itens as $item ) :
								$n++;
								extract( $item, EXTR_PREFIX_ALL, 'item' );
								?>
								<div class="<?= $classeCols ?>">
									<article class="parceiros_item anim" <?php animAttr( 'zoomIn', 0.6, incCol( 0.4, $n, 6 ) ) ?>>
										<?php
										img( $item_logo, 'medium', cl('parceiro_logo') );
										hotspot( $item_link, AUTO );
										?>
									</article>
								</div><!-- .col -->
								<?php
								row( $n, $classeCols );
							endforeach;
							?>
						</div><!-- .row -->
					</div><!-- #post_parceiros -->
				<?php endif; ?>
				
			</div><!-- .entry-content -->
		</div><!-- .entry-main -->
	</div><!-- .container -->
	
</article><!-- #post-## -->

<?php

get_footer();
