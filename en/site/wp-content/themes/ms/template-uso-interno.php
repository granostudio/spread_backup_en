<?php
/**
 * Template Name: Uso Interno
 *
 * @package MS
 */
 
global $post;

$templateEspecifico = get_template_directory() . '/page-' . $post->post_name . '.php';

if ( file_exists( $templateEspecifico ) )
	require $templateEspecifico;
else
	wp_redirect( site_url() );
