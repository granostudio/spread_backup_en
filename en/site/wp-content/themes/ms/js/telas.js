/*
 * Controla a troca de telas
 *
 * Desenvolvido pela MontarSite, 2017 - http://www.montarsite.com.br
 */

function ajustarAlturaTela() {
	// Ajusta a altura do container para a tela atual
	var $d = this.data.bind(this),
		altura = $d('$telaAtual').height() + $d('containerPadding');
	this.css({
		'height' : altura + 'px'
	});
	this.ajustarColunas();
}

function ajustarColunasTela() {
	var $d = this.data.bind(this),
		$colunas = $d('$telaAtual').find('.tela_coluna'),
		maiorColuna = 0;
	if ( $colunas.length ) {
		// Equaliza a altura de colunas nesta tela
		$colunas
			.css( 'minHeight', 0 )
			.each( function(n) {
				var $col = $colunas.eq(n),
					// altura = $this.height();
					altura = Math.ceil( $col.prop('offsetHeight') + 1 ); // O +1 cobre eventuais frações de pixel que não são reportadas pelo offsetHeight
				maiorColuna = Math.max( maiorColuna, altura );
			})
			.each( function(n) {
				var $col = $colunas.eq(n);
				$col.css( 'minHeight', maiorColuna + 'px' );
			});
	}
}

function trocarTela( indice ) {
	var $d = this.data.bind(this),
		$telaAlvo,
		mov;
	if ( indice == $d('telaAtual') || indice > $d('totalTelas') )
		return false;
	// Operações iniciais
	$d('$telaAtual').removeClass('ativa');
	$telaAlvo = $d('$telas').eq( indice );
	$telaAlvo.addClass('ativa');
	$d( 'telaAtual', indice );
	$d( '$telaAtual', $telaAlvo );
	mov = ( ( $d('larguraTela') + $d('margemTela') ) * indice * -1 ) + 'px';
	// Move a primeira tela e, consequentemente, todas as demais
	// $telas.eq(0).css( 'marginLeft', mov );
	$d('$wrapper').css( 'transform', 'translateX(' + mov + ')' );
	// Rola para o início da tela
	// window.scroll( 0, $d('$wrapper').offset().top );
	// Ajusta a altura do container para a nova tela
	this.ajustarAltura();
}



// Setup

+function() {
	
	jQuery('.telas').each( function() {
		
		// Preparação das telas

		var $container = jQuery(this),
			$d = $container.data.bind($container),
			ipad = /iPad/.test( navigator.userAgent );
		
		$d({
			containerPadding	: parseInt( $container.css('paddingTop') ) + parseInt( $container.css('paddingBottom') ),
			$wrapper			: $container.find('.telas_wrapper'),
			$telas				: $container.find('.tela'),
			telaAtual			: 0,
		});
		
		$container[0].$ = $container;
		$container.trocar = trocarTela.bind( $container );
		$container.ajustarAltura = ajustarAlturaTela.bind( $container );
		$container.ajustarColunas = ajustarColunasTela.bind( $container );
		
		$d( 'larguraTela', $d('larguraTela') || $d('$wrapper').width() );
		$d( 'margemTela', parseInt( $d('$telas').eq(0).css('marginRight') ) );
		$d( 'totalTelas', $d('$telas').length );
		$d( '$telaAtual', $d('$telas').eq(0) );

		// Ajusta o wrapper para conter todas as telas
		$d('$wrapper').css({
			'width' : ( $d('totalTelas') * ( $d('larguraTela') + $d('margemTela') ) ) + 'px',
		});

		// Ajusta as telas para a largura original do container
		$d('$telas').each( function(n,el) {
			var $this = $d('$telas').eq(n);
			if ( n == 0 )
				$this.addClass('ativa');
			$this.css( 'width', $d('larguraTela') + 'px' );
		} );
		
		if ( ipad )
			setTimeout( $container.ajustarAltura, 2000 );
		else
			$container.ajustarAltura();
		
	} );
	
}(); //jQuery( );
