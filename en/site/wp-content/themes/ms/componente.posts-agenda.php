<?php
// @requer classe Elemento, animAttr, col, row, thumbFundo, imgTema, linksCompartilhamento, gerarResumo
if ( $param_posts ) :
	?>

	<div <?= $this->attr( $this->_attrWrapper ) ?>>
		<div class="row<?php if ( $param_colunas > 1 ) print ' equalizar_lista' ?>">
			<?php
			$n = 0;
			$classeCols = 'col-xs-12 col-sm-' . col( $param_colunas );
			foreach ( $param_posts as $p ) :
				$n++;
				$post_link = get_the_permalink( $p->ID );
				$data_evento = strtotime( get_field( 'data_evento', $p->ID ) );
				$organizador = get_field( 'organizador', $p->ID );
				?>
				<div class="post_col <?= $classeCols ?>">
					<article <?= $this->attr( 'class', null, 'post' ) ?> <?php if ( $param_usar_thumb ) thumbFundo( $p->ID, 'medium' ) ?>>
						<div class="post_conteudo">
							<p class="post_meta">
								<?=
								E::span( strftime( '%b', $data_evento ), 'mes' ) .
								E::span( strftime( '%d', $data_evento ), 'dia' ) .
								E::span( strftime( '%Y', $data_evento ), 'ano' )
								?>
							</p><!-- .post_meta -->
							<?=
							E::p( 'Organizador:' . ' ' . $organizador, 'organizador' ) .
							E::h2( null, 'post_titulo' )
								->a( $post_link, $p->post_title, E::AUTO, 'post_titulo_link' ) .
							E::p( gerarResumo( 150, $p ), 'post_resumo' )
							?>
							<ul class="post_compartilhar">
								<?php
								linksCompartilhamento( array(
									'url'		=> $post_link,
									'titulo'	=> $p->post_title,
									'resumo'	=> gerarResumo( 60, $p ),
								) );
								?>
							</ul><!-- .post_compartilhar -->
							<?=
							E::a(
								$post_link,
								'Saiba Mais' . imgTema( 'mais_branco.png', 'Saiba Mais', 'post_mais_img' ),
								AUTO,
								'post_mais'
							)
							?>
						</div><!-- .post_conteudo -->
					</article><!-- .post_item -->
				</div><!-- .col -->
				<?php
				if ( $param_colunas > 1 ) {
					row( $n, $classeCols );
				}
			endforeach;
			?>
		</div><!-- .row -->
	</div><!-- .posts_blog -->

<?php elseif ( $param_msg_sem_posts ) : ?>

	<p class="sem_posts"><?= is_search() ? 'Não há posts que correspondem com sua busca.' : 'Não há posts nesta seção.' ?></p>

<?php endif; ?>
