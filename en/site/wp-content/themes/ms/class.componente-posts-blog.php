<?php

class Componente_Posts_Blog extends Componente_Abstrato {
	
	protected $_attrWrapper = array( 'id', 'class' );
	
	public function __construct( $novosParams = null ) {
		// Configurações do componente
		$this->_nome = 'Posts do Blog';
		$this->_slug = 'posts_blog';
		$this->_aceitarParamsNovos = false;
		$this->_padroes = array(
			'id'				=> null,
			'class'				=> 'posts_blog',
			'posts'				=> null,
			'query'				=> null,
			'colunas'			=> 3,
			'msg_sem_posts'		=> true,
		);
		$this->_sanitizadores = array(
			'colunas'			=> 'intval',
		);
		$this->_obrigatorios = array(
		);
		$this->_validadores = array(
			'colunas'			=> 'naoVazio',
			'posts'				=> 'is_array',
			'query'				=> 'is_array',
			'id'				=> 'is_string',
			'class'				=> 'is_string',
		);
		$this->_regrasJuncao = array(
			'class'				=> '+',
		);
		// Inicialização
		parent::__construct( $novosParams );
	}
	
	public function renderizar( $_imprimir = true ) {
		
		global $wp_query;
		
		// Se presente uma query, roda-a para obter os posts
		if ( $this->query )
			$this->posts = get_posts( $this->query );
		// Por padrão, obtém os posts da query atual
		elseif ( !$this->posts )
			$this->posts = $wp_query->posts;
				
		return parent::renderizar( $_imprimir );
		
	}
		
}