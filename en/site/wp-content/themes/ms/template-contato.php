<?php
/**
 * Template Name: Contato
 */
global $post;

get_header();

extract( get_fields() );

?>
<article id="contato">
	<?php
	
	
	
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// INTRO
	
	// extract( $info[0], EXTR_PREFIX_ALL, 'info' );

	?>
	<section id="contato_info" <?php fundo( $fundo, 'full' ) ?> class="secao">
		<div class="secao_wrapper">
			<div class="container">
				<?php
				$compParams =& $info[0];
				$compParams['form'] = $form;
				Componente::infoContato( $compParams );
				?>
			</div><!-- .container -->
		</div><!-- .secao_wrapper -->
	</section><!-- #contato_info -->
	<?php
	
	
	
	// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// ENDEREÇOS

	if ( $enderecos ) :
		?>
		<section id="contato_enderecos" class="secao equalizar_lista">
			<div class="secao_wrapper">
				<div class="container">
					<div class="row inline">
						<?php
						$classeCols = 'col-xs-12 col-sm-6 col-md-4';
						$n = 0;
						foreach ( $enderecos as $item ) :
							$n++;
							extract( $item, EXTR_PREFIX_ALL, 'item' );
							?>
							<div class="<?= $classeCols ?>">
								<section class="enderecos_item">
									<?=
									E::h3( $item_cidade, 'item_cidade' ) .
									E::div( $item_embed, 'item_mapa' ) .
									E::address( $item_endereco, 'item_endereco equalizar_item' ) .
									E::p( 'Telefone: ' . $item_telefone, 'item_telefone' )
									?>
								</section><!-- .enderecos_item -->
							</div><!-- .col -->
							<?php
							row( $n, $classeCols );
						endforeach;
						?>
					</div><!-- .row -->
				</div><!-- .container -->
			</div><!-- .secao_wrapper -->
		</section><!-- #contato_enderecos -->
		<?php
	endif;
	
	
	?>
</article><!-- #contato -->
<?php
get_footer();
