<?php
/*
 * @param (array_n) opcoes = null
 * @param (string) selecionado = ''
 * @param (string ou false) placeholder = '--- Escolha ---'
 * @param (string) name = null
 * @param (string) id = null
 * @param (string) class = null
 * @param (string ou false) attrChave = value
 *
 * @requer listaOptions, obter, attr
 */
 
if ( !isset( $params['opcoes'] ) || !is_array( $param_opcoes ) )
	return print '[Componente Select] Sem opções definidas.';
 
$attrPadroes = array(
	'id'		=> null,
	'name'		=> null,
);
 
$param_selecionado = obter( 'selecionado', $params, !isset( $params['placeholder'] ) || $params_placeholder !== false ? null : key( $param_opcoes ) );

?>
<div class="select_wrapper<?php if ( isset( $params['class'] ) ) print ' ' . $param_class ?>">
	<select <?= attr( $attrPadroes, $params ) ?>>
		<?php if ( !isset( $params['placeholder'] ) || $param_placeholder !== false ) : ?>
			<option value="" <?php if ( !$param_selecionado ) print 'selected' ?>><?= esc_html( obter( 'placeholder', $params, '--- Escolha ---' ) ) ?></option>
		<?php endif; ?>
		<?php if ( obter( 'opcoes', $params ) ) listaOptions( $param_opcoes, $param_selecionado, isset( $params['attrChave'] ) && is_string( $param_attrChave ) ? array( 'attrChave' => $param_attrChave ) : null ); ?>
	</select>
</div><!-- .select_wrapper -->