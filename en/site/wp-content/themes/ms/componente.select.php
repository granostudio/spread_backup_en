<?php
/*
 * @requer listaOptions
 */
?>
<div <?= $this->attr( $this->_attrWrapper, null, 'wrapper' ) ?>>
	<select <?= $this->attr( $this->_attrSelect ) ?>>
		<?php
		if ( $param_placeholder ) :
			?>
			<option value="" <?php if ( $param_selecionado === false ) print 'selected' ?>><?= esc_html( $param_placeholder ) ?></option>
			<?php
		endif;
		listaOptions(
			$param_opcoes,
			$param_selecionado,
			array( 'attrChave' => $param_attrChave )
		);
		?>
	</select><!-- .select -->
</div><!-- .select_wrapper -->