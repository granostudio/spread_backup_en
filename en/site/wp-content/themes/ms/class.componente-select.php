<?php

class Componente_Select extends Componente_Abstrato {
	
	protected $_attrWrapper = array( 'id', 'class' );
	protected $_attrSelect = array( 'id', 'class', 'name', 'disabled' );
	
	public function __construct( $novosParams = null ) {
		// Configurações do componente
		$this->_nome = 'Select';
		$this->_slug = 'select';
		$this->_aceitarParamsNovos = false;
		$this->_padroes = array(
			'id'				=> null,
			'name'				=> null,
			'class'				=> 'select',
			'wrapper_id'		=> null,
			'wrapper_class'		=> 'select_wrapper',
			'placeholder'		=> '--- Escolha ---',
			'opcoes'			=> null,
			'lista'				=> null,
			'selecionado'		=> null,
			'attrChave'			=> 'value',
			'disabled'			=> null,
		);
		$this->_sanitizadores = array(
		);
		$this->_obrigatorios = array(
		);
		$this->_validadores = array(
			'opcoes'			=> 'is_array',
			'lista'				=> 'is_string',
			'attrChave'			=> 'is_string',
		);
		$this->_regrasJuncao = array(
			'class'				=> '+',
		);
		// Inicialização
		parent::__construct( $novosParams );
	}
	
	public function renderizar( $_imprimir = true ) {
		
		if ( $this->lista && class_exists('Listas') )
			// @requer classe Listas
			$this->opcoes = Listas::obter( $this->lista );
		
		/*
		if ( !$this->opcoes ) {
			$this->_mensagem('Sem opções a listar.');
			$this->_permitirOutput = false;
			return false;
		}
		*/
		
		// Se um dos parâmetros foi dado, mas não o outro, o duplica
		if ( $this->id && !$this->name )
			$this->name = $this->id;
		elseif ( $this->name && !$this->id )
			$this->id = $this->name;
		
		if ( !$this->selecionado ) {
			// Por padrão, seleciona o placeholder, se presente, ou o primeiro item
			$this->selecionado = $this->placeholder
				? false
				: ( $this->opcoes ? key( $this->opcoes ) : null )
			;
		}
		
		return parent::renderizar( $_imprimir );
		
	}
		
}