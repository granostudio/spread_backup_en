<?php

class Componente_Telas extends Componente_Abstrato {
	
	protected $_attrWrapper = array( 'id', 'class' );
	
	public function __construct( $novosParams = null ) {
		// Configurações do componente
		$this->_nome = 'Telas';
		$this->_slug = 'telas';
		$this->_aceitarParamsNovos = false;
		$this->_padroes = array(
			'id'				=> null,
			'class'				=> 'telas',
			'telas'				=> null,
		);
		$this->_sanitizadores = array(
		);
		$this->_obrigatorios = array(
			'telas'				=> 1,
		);
		$this->_validadores = array(
			'telas'				=> 'is_array',
			'id'				=> 'is_string',
			'class'				=> 'is_string',
		);
		$this->_regrasJuncao = array(
			'class'				=> '+',
		);
		// Inicialização
		parent::__construct( $novosParams );
	}
		
}