<?php

class Componente_Paginacao extends Componente_Abstrato {
	
	protected $_attrWrapper = array( 'id', 'class' );
	protected $_totalPaginas = null;
	protected $_temAnterior = null;
	protected $_temProxima = null;
	
	public function __construct( $novosParams = null ) {
		// Configurações do componente
		$this->_nome = 'Paginacao';
		$this->_slug = 'paginacao';
		$this->_aceitarParamsNovos = false;
		$this->_padroes = array(
			'id'				=> null,
			'class'				=> 'paginacao',
			'param_paginacao'	=> null,
			'pagina'			=> null,
			'total_itens'		=> null,
			'itens_por_pagina'	=> null,
			'url'				=> null,
			'msg_fim'			=> true,
		);
		$this->_sanitizadores = array(
		);
		$this->_obrigatorios = array(
		);
		$this->_validadores = array(
			'id'				=> 'is_string',
			'class'				=> 'is_string',
			'param_paginacao'	=> 'is_string',
			'pagina'			=> 'is_int',
			'total_itens'		=> 'is_int',
			'itens_por_pagina'	=> 'is_int',
			'url'				=> 'is_string',
		);
		$this->_regrasJuncao = array(
			'class'				=> '+',
		);
		// Inicialização
		parent::__construct( $novosParams );
		self::_posInit();
	}
	
	private function _posInit() {
		
		if ( !$this->url )
			$this->url = add_query_arg( $_GET, home_url( $wp->request ) );
		
	}
	
	public function linkPagina( $pagina ) {
		
		if ( $this->param_paginacao ) {
			return esc_url( add_query_arg( $this->param_paginacao, $pagina, $this->url ) );
		} else {
			$posQuery = strpos( $this->url, '?' );
			$url = $posQuery
				? substr( $this->url, 0, $posQuery )
				: $this->url
			;
			$query = $posQuery
				? substr( $this->url, $posQuery )
				: ''
			;
			return esc_url( strpos( $this->url, '/page/' ) !== false
				? preg_replace( '#/page/\d+#', '/page/' . $pagina, $this->url )
				: trailingslashit( $url ) . 'page/' . $pagina . '/' . $query
			);
		}
		
	}
	
	public function renderizar( $_imprimir = true ) {
		
		// @requer umDe, obter
		
		global $wp_query;
		
		if ( !$this->pagina )
			$this->pagina = (int) umDe( $this->param_paginacao ? obter( $this->param_paginacao, $_GET ) : $wp_query->get('paged'), 1 );
		if ( !$this->total_itens )
			$this->total_itens = (int) $wp_query->found_posts;
		if ( !$this->itens_por_pagina )
			$this->itens_por_pagina = (int) umDe( get_option('posts_per_page'), $wp_query->post_count );
		
		$this->_totalPaginas = ceil( $this->total_itens / $this->itens_por_pagina );
		$this->_temAnterior = $this->pagina > 1;
		$this->_temProxima = $this->pagina < $this->_totalPaginas;
		
		return parent::renderizar( $_imprimir );
		
	}
		
}