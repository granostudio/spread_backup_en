<?php
extract( get_fields('widget_acf_widget_4'), EXTR_PREFIX_ALL, 'rodape' );
$templatesIgnorados = array(
	'template-contato.php',
);
?>


			<?php flushDump(); ?>

		</main><!-- #content-area -->
	</div><!-- #content -->



	<?php if ( !is_front_page() && is_page() && !in_array( get_post_meta( get_the_ID(), '_wp_page_template', true ), $templatesIgnorados ) ) : ?>

		<aside id="rodape_blog">
			<div id="rodape_blog_wrapper" class="secao_wrapper">
				<div class="container">
					<?php Componente::sliderBlog(); ?>
				</div><!-- .container -->
			</div><!-- #rodape_blog_wrapper -->
		</aside><!-- #rodape_blog -->

		<aside id="rodape_info">
			<div class="secao_wrapper">
				<div class="container">
					<?php Componente::infoContato(); ?>
				</div><!-- .container -->
			</div><!-- .secao_wrapper -->
		</aside><!-- #rodape_info -->

	<?php endif; ?>



	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="container">

			<div id="rodape_linha1">
				<div class="row">

					<div class="col-xs-12 col-sm-3">
						<div id="rodape_logo">
							<a href="<?= site_url() ?>"><?php img( $rodape_logo, 'medium', cl('rodape_logo_img') ) ?></a>
						</div><!-- #rodape_logo -->
					</div><!-- .col -->

					<div class="col-xs-12 col-sm-2">
						<div id="rodape_col1">
							<?= do_shortcode( str_replace( '&quot;', '"', $rodape_coluna1 ) ) ?>
						</div><!-- #rodape_col1 -->
					</div><!-- .col -->

					<div class="col-xs-12 col-sm-2">
						<div id="rodape_col2">
							<?= do_shortcode( str_replace( '&quot;', '"', $rodape_coluna2 ) ) ?>
						</div><!-- #rodape_col2 -->
					</div><!-- .col -->

					<div class="col-xs-12 col-sm-2">
						<div id="rodape_col3">
							<?= do_shortcode( str_replace( '&quot;', '"', $rodape_coluna3 ) ) ?>
							<ul class="menu">
								<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="#" class="btn-newsletter" style="color: #FEA32E!important;text-decoration: underline;">Newsletter</a></li>
							</ul>
						</div><!-- #rodape_col3 -->
					</div><!-- .col -->

					<div class="col-xs-12 col-sm-3">
						<div id="rodape_contato">

							<p class="rodape_telefone"><?= esc_html( $rodape_telefone ) ?></p>
							<p class="rodape_email"><?php ofuscar( $rodape_email ) ?></p>
							<address class="rodape_endereco"><?= $rodape_endereco ?></address>

						</div><!-- #rodape_contato -->
					</div><!-- .col -->

				</div><!-- .row -->
			</div><!-- #rodape_linha1 -->

			<div id="rodape_linha2" class="vert_ancestral">
				<div class="row">

					<div class="col-xs-12 col-sm-9">
						<p id="rodape_copyright" class="vert_item"><?= esc_html( $rodape_copyright ) ?> | Desenvolvido por Grano Studio</p>
					</div><!-- .col -->

					<div class="col-xs-12 col-sm-3">
						<nav id="rodape_redes" class="vert_item">
							<?php wp_nav_menu( array( 'theme_location' => 'social', 'menu_class' => 'menu_social' ) ); ?>
						</nav><!-- #rodape_redes -->
					</div><!-- .col -->

				</div><!-- .row -->
			</div><!-- #rodape_linha2 -->

		</div><!-- .container -->
	</footer><!-- #colophon -->

	<?php dynamic_sidebar('rodape_scripts') ?>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
