<?php

class Componente_Slider_Blog extends Componente_Abstrato {
	
	public function __construct( $novosParams = null ) {
		// Configurações do componente
		$this->_nome = 'Slider do Blog';
		$this->_slug = 'slider_blog';
		$this->_aceitarParamsNovos = false;
		$this->_padroes = array(
			'id'				=> 'slider_blog',
			'class'				=> 'slider_blog',
			'titulo'			=> null,
			'intro'				=> null,
			'ordem'				=> null,
			'sticky_posts'		=> null,
			'qtd_posts'			=> 9,
			'colunas'			=> 3,
		);
		$this->_sanitizadores = array(
			'qtd_posts'			=> 'intval',
			'colunas'			=> 'intval',
		);
		$this->_obrigatorios = array(
			'colunas'			=> true,
			// 'titulo'			=> true,
			// 'ordem'				=> true,
			// 'qtd_posts'			=> true,
		);
		$this->_validadores = array(
			'colunas'			=> 'naoVazio',
			'id'				=> 'is_string',
			'class'				=> 'is_string',
			'titulo'			=> 'is_string',
			'intro'				=> 'is_string',
			'ordem'				=> 'is_string',
		);	
		$this->_regrasJuncao = array(
			'class'				=> '+',
		);
		// Inicialização
		parent::__construct( $novosParams );
	}
	
	public function renderizar( $_imprimir = true ) {
		
		// @requer obter
		
		// Carrega os sticky posts
		if ( !$this->sticky_posts )
			$this->sticky_posts = get_option('sticky_posts');
		
		// Carrega os campos do ACF da Home
		if ( !$this->ordem )
			$this->configurar( obter( 0, get_field( 'blog', PAGINA_HOME ) ) );
				
		return parent::renderizar( $_imprimir );
		
	}
		
}