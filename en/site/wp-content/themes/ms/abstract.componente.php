<?php

final class Componente {
	
	static private $_ids = array();
	static private $_caminhoBase = false;
	static private $_instancias = array();
	
	// @var string = Configura um subdiretório dentro do tema, de onde os componentes serão carregados
	static public $diretorioBase = '';
	
	static public function slug( $nome ) {
		// Retorna um slug a partir de um nome grafado de outra forma
		return strtolower( preg_replace( '/[_ ]/', '-', $nome ) );
	}
	
	static public function classe( $nome ) {
		// Retorna o nome da classe a partir de um nome grafado de outra forma
		return 'Componente_' . str_replace( ' ', '_', ucwords( preg_replace( '/[-_]/', ' ', strtolower( $nome ) ) ) );
	}
	
	static public function idUnico( $nome ) {
		// Retorna um ID único para uma nova instância de $nome
		$slug = self::slug( $nome );
		if ( !isset( self::$_ids[ $slug ] ) )
			self::$_ids[ $slug ] = -1;
		return $slug . ++self::$_ids[ $slug ];
	}
	
	static public function ultimoIndice( $nome ) {
		$slug = self::slug( $nome );
		return self::$_ids[ $slug ];
	}
	
	static public function caminho( $endpoint = '' ) {
		// Retorna o caminho real completo para um $endpoint dentro do diretório base do tema / dos componentes (se configurado)
		if ( !self::$_caminhoBase )
			self::$_caminhoBase = get_template_directory();
		return realpath( self::$_caminhoBase . DIRECTORY_SEPARATOR . self::$diretorioBase . DIRECTORY_SEPARATOR . $endpoint );
	}
	
	static public function extrairCaminhoRelativo( $caminho ) {
		// Remove o caminho do tema do $caminho dado
		return str_replace( self::$_caminhoBase . DIRECTORY_SEPARATOR, '', $caminho );
	}
	
	static public function caminhoDoTemplate( $nome ) {
		// Retorna um caminho de sistema padroniado para templates de componentes
		$slug = self::slug( $nome );
		return self::caminho( 'componente.' . $slug . '.php' );
	}
	
	static public function caminhoDaClasse( $nome ) {
		// Retorna um caminho de sistema padronizado para classes de componentes
		$slug = self::slug( $nome );
		return self::caminho( 'class.componente-' . $slug . '.php' );
	}
	
	static public function criar( $nome, $params = null ) {
		
		// Instancia um componente e repassa os $params
		
		// Definições
		$nomeClasse = self::classe( $nome );
		$slug = self::slug( $nome );
		$caminhoDoTemplate = self::caminhoDoTemplate( $slug );
		
		// Verifica o template
		if ( !file_exists( $caminhoDoTemplate ) )
			return self::_mensagem( 'Template "%s" não encontrado para o componente "%s".',
				'componente.' . $slug . '.php',
				$nomeClasse
			);
		
		// Verifica a classe
		if ( !class_exists( $nomeClasse, false ) ) {
			$caminhoDaClasse = self::caminhoDaClasse( $slug );
			if ( !file_exists( $caminhoDaClasse ) )
				return self::_mensagem( 'Classe do componente "%s" não carregada e não encontrada em "%s".',
					$nomeClasse,
					self::extrairCaminhoRelativo( $caminhoDaClasse )
				);
			require $caminhoDaClasse;
		}
		
		// Gera e armazena a instância
		if ( !isset( self::$_instancias[ $slug ] ) )
			self::$_instancias[ $slug ] = array();
		
		return self::$_instancias[ $slug ][] = new $nomeClasse( $params );
		
	}
	
	static public function obterInstanciaPeloIndice( $nome, $indice ) {
		// Retorna um componente instanciado pelo seu $nome e $indice numérico
		$slug = self::slug( $nome );
		if ( !isset( self::$_instancias[ $slug ] ) || !isset( self::$_instancias[ $slug ][ $indice ] ) )
			return false;
		return self::$_instancias[ $slug ][ $indice ];
	}
	
	static public function obterInstanciaPeloId( $id ) {
		// Retorna um componente instanciado pelo seu $id (composto pelo slug e índice numérico)
		$slug = preg_replace( '/\d+$/', '', $id );
		$indice = (int) preg_replace( '/^\D+/', '', $id );
		return self::obterInstanciaPeloIndice( $slug, $indice );
	}
	
	static public function renderizar( $nome, $params = null, $imprimir = true ) {
		// Instancia um componente e já o renderiza
		$instancia = self::criar( $nome, $params );
		if ( !$instancia )
			return false;
		return $instancia->renderizar( $imprimir );
	}
	
	static public function mensagem( $nomeComponente, $string ) {
		// Função genérica para imprimir mensagens de erro, passando um sprintf na $string caso haja mais argumentos
		$argCount = func_num_args();
		if ( $argCount > 2 ) {
			$args = func_get_args();
			array_shift( $args );
			$string = call_user_func_array( 'sprintf', $args );
		}
		print "[$nomeComponente] $string<br />\n";
		return false;
	}
	
	static private function _mensagem() {
		// Gera mensagens para esta classe
		return call_user_func_array( array( self, 'mensagem' ), array_merge( array( 'Fábrica de Componentes' ), func_get_args() ) );
	}
	
	static public function hifenizar( $chave ) {
		if ( !preg_match( '#[A-Z_\\. /\\\\]#', $chave ) )
			return $chave;
		$chave = preg_replace_callback( '/[A-Z]/', function($matches) { return '-' . strtolower( $matches[0] ); }, $chave );
		$chave = preg_replace( '#[ /\\\\_\\.]+#', '-', $chave );
		$chave = preg_replace( '#-{2,}#', '-', $chave );
		return $chave;
	}
	
	static public function __callStatic( $func, $args ) {
		$func = self::hifenizar( $func );
		array_unshift( $args, $func );
		return call_user_func_array( array( self, 'renderizar' ), $args );
	}
	
}



abstract class Componente_Abstrato {
	
	// Constantes
	
	const ATTR_MODO_BUSCA = 1;
	const ATTR_MODO_ARRAY = 2;
	const ATTR_MODO_EXCLUSAO = 4;
	const ATTR_PREFIXAR_DATA = 8;
	const ATTR_MODO_PADRAO = self::ATTR_MODO_BUSCA;
	
	// Variáveis internas fixas
	
	protected $_params = array();
	protected $_permitirOutput = true;
	protected $_id = '';
	protected $_indice = null;
	
	// Variáveis substituíveis pelo componente
	
	protected $_nome = 'Abstrato';
	protected $_slug = 'abstrato';
	protected $_aceitarParamsNovos = false;
	protected $_validarAoConfigurar = true;
	
	// @var array $chave => $valor
	protected $_padroes = array();
	// @var array $chave => (string ou callable) $regra ( '=', '+' ou callable )
	protected $_regrasJuncao = array();
	// @var array $chave => callable $func
	protected $_sanitizadores = array();
	// @var array $chave => callable $func
	protected $_validadores = array();
	// @var array $chave => (bool) $obrigatoriedade
	protected $_obrigatorios = array();
	
	
	
	// Métodos do componente
	
	public function __construct( $novosParams = null ) {
		
		// Instanciação
		
		// Gera o ID único dessa instância
		$this->_id = Componente::idUnico( $this->_slug );
		$this->_indice = Componente::ultimoIndice( $this->_slug );
		
		// Popula com os parâmetros padrão
		$this->_params = $this->_padroes;
		
		// Processa os novos
		if ( $novosParams && is_array( $novosParams ) )
			$this->configurar( $novosParams );
		
	}
	
	public function __set( $chave, $valor ) {
		// Altera um parâmetro
		return $this->configurar( array( $chave => $valor ) );
	}
	
	public function __get( $chave ) {
		// Retorna o valor de um parâmetro
		return $this->_params[ $chave ];
	}
	
	public function __unset( $chave ) {
		// Reseta um parâmetro
		if ( array_key_exists( $chave, $this->_params ) )
			$this->_params[ $chave ] = null;
	}
	
	public function __isset( $chave ) {
		// Verifica se um parâmetro contém um valor não nulo
		return isset( $this->_params[ $chave ] );
	}
	
	public function obterId() {
		// Retorna a ID dessa instância
		return $this->_id;
	}
	
	public function obterIndice() {
		// Retorna a ID dessa instância
		return $this->_indice;
	}
	
	public function obterSlug() {
		// Retorna o slug desse componente
		return $this->_slug;
	}
	
	public function estáAtivo() {
		// Informa se o componente possuirá algum output
		return $this->_permitirOutput;
	}
	
	public function configurar( array $novosParams ) {
		
		// Altera os parâmetros do componente
		
		foreach ( $novosParams as $chave => $valor ) {
			
			// Chaves não existentes
			if ( !$this->_aceitarParamsNovos && !array_key_exists( $chave, $this->_params ) )
				continue;
			
			// Sanitização do valor
			if ( isset( $this->_sanitizadores[ $chave ] ) && is_callable( $this->_sanitizadores[ $chave ] ) )
				// @param $valorNovo
				// @param $chave
				// @return $valorLimpo
				$valor = call_user_func( $this->_sanitizadores[ $chave ], $valor ); // , $chave
				
			// Chaves sem regra específica
			if ( !isset( $this->_regrasJuncao[ $chave ] ) ) {
				$this->_params[ $chave ] = $valor;
				continue;
			}
			
			// Chaves com regra
			switch ( $this->_regrasJuncao[ $chave ] ) {
				case '=' :
					$this->_params[ $chave ] = $valor;
				break;
				case '+' :
					if ( is_string( $this->_params[ $chave ] ) )
						$this->_params[ $chave ] .= ' ' . $valor;
					elseif ( is_array( $this->_params[ $chave ] ) )
						$this->_params[ $chave ] = array_merge( $this->_params[ $chave ], $valor );
					else
						$this->_params[ $chave ] += $valor;
				break;
				default :
					if ( is_callable( $this->_regrasJuncao[ $chave ] ) )
						// @param $valorAtual
						// @param $valorNovo
						// @return $valorFinal
						$this->_params[ $chave ] = call_user_func( $this->_regrasJuncao[ $chave ], $this->_params[ $chave ], $valor );
				break;
			}
			
		}
		
		// Valida
		if ( $this->_validarAoConfigurar )
			$this->validar();
		
		return $this->_permitirOutput;
		
	}
	
	public function exportar() {
		// Retorna todos os parâmetros atuais
		return $this->_params;
	}
	
	public function validar() {
		
		// Valida os parâmetros atuais
		
		$status = true;
		
		foreach ( $this->_params as $chave => $valor ) {
			
			// Obrigatoriedade
			if ( isset( $this->_obrigatorios[ $chave ] ) && (bool) $this->_obrigatorios[ $chave ] && is_null( $valor ) ) {
				$status = false;
				$this->_mensagem('Parâmetro "%s" não pode ser nulo!', $chave);
				continue;
			}
			
			// Validação específica
			if ( !is_null( $valor ) && isset( $this->_validadores[ $chave ] ) && is_callable( $this->_validadores[ $chave ] ) ) {
				// @param $valor
				// @param $chave // REMOVIDO
				// @return boolean / string $mensagemErro
				$validacao = call_user_func( $this->_validadores[ $chave ], $valor ); // , $chave
				if ( !$validacao || is_string( $validacao ) )
					$status = false;
				if ( is_string( $validacao ) )
					$this->_mensagem( $validacao );
				elseif ( $validacao === false )
					$this->_mensagem('O campo "%s" possui um valor inválido.', $chave);
			}
			
		}
		
		return $this->_permitirOutput = $status;
		
	}
	
	protected function _mensagem() {
		// Gera uma mensagem específica para este componente
		return call_user_func_array( array( Componente, 'mensagem' ), array_merge( array( $this->_nome ), func_get_args() ) );
	}
	
	public function renderizar( $_imprimir = true ) {
		
		// Chama o template desse componente e torna os parâmetros disponíveis no contexto local
		
		// Verificações
		
		if ( !$this->_permitirOutput )
			return false;
		
		$_nomeTemplate = Componente::caminhoDoTemplate( $this->_slug );

		/* // Verificação realizada na instanciação do componente na classe mestre
		if ( !file_exists( $nomeTemplate ) ) {
			$this->_mensagem("Template \"$nomeTemplate\" não encontrado!");
			return false;
		}
		*/
		
		if ( !$_nomeTemplate )
			return false;
		
		// Variáveis locais disponibilizadas ao template de forma mais simples
		
		// $param_id = $this->_id;
		// $param_slug = $this->_slug;
		extract( $this->_params, EXTR_PREFIX_ALL, 'param' );
		
		// Inclusão
		
		if ( !$_imprimir )
			ob_start();
		
		require $_nomeTemplate;
		
		if ( !$_imprimir ) {
			$_htmlTemplate = ob_get_contents();
			ob_end_clean();
			return $_htmlTemplate;
		} else {
			return true;
		}
		
	}
	
	
	
	// Métodos auxiliares para o template
	
	public function hifenizar( $chave ) {
		if ( !preg_match( '#[A-Z_\\. /\\\\]#', $chave ) )
			return $chave;
		$chave = preg_replace_callback( '/[A-Z]/', function($matches) { return '-' . strtolower( $matches[0] ); }, $chave );
		$chave = preg_replace( '#[ /\\\\_\\.]+#', '-', $chave );
		$chave = preg_replace( '#-{2,}#', '-', $chave );
		return $chave;
	}
	
	public function attrId() {
		// Imprime o HTML com o ID do componente como atributo
		print "id=\"$this->_id\"";
	}
	
	public function attr( $atributos, $opcoes = self::ATTR_MODO_PADRAO, $prefixo = false ) {
		
		// Busca os $atributos dentre os parâmetros, com o $prefixo opcional, e imprime-os em HTML (sem o prefixo)
		// Se $data for true, as chaves serão prefixadas por 'data-' no HTML (ex: target = data-target)
		// Atributos em camel case serão hifenizados (ex: dataTarget = data-target)
		
		if ( is_string( $atributos ) )
			$atributos = array( $atributos );
		elseif ( !is_array( $atributos ) || !count( $atributos ) )
			return false;
			
		if ( is_null( $opcoes ) )
			$opcoes = self::ATTR_MODO_PADRAO;
		
		if ( $prefixo )
			$prefixo = rtrim( $prefixo, '_' ) . '_';
		
		// Obtém os valores a serem utilizados
		
		$arrValores = array();
		
		if ( self::ATTR_MODO_BUSCA & $opcoes ) {
			// Extrai dos params chaves presentes em $atributos
			// Se o prefixo foi dado, busca as chaves com o prefixo e o remove delas
			foreach ( $atributos as $chave ) {
				$chaveBusca = $prefixo . $chave;
				if ( !isset( $this->_params[ $chaveBusca ] ) )
					continue;
				$chaveFinal = ( self::ATTR_PREFIXAR_DATA & $opcoes ? 'data-' : '' ) . $this->hifenizar( $chave );
				$arrValores[ $chaveFinal ] = $this->_params[ $chaveBusca ];
			}
		} elseif ( self::ATTR_MODO_ARRAY & $opcoes ) {
			// Utiliza valores fornecidos em $atributos, sem busca nos params
			$atributos = array_map( array( $this, 'hifenizar' ), $atributos );
			if ( self::ATTR_PREFIXAR_DATA & $opcoes ) {
				foreach ( $atributos as $chave => $valor )
					$arrValores[ 'data-' . $chave ] = $valor;
			} else {
				$arrValores =& $atributos;
			}
		} elseif ( self::ATTR_MODO_EXCLUSAO & $opcoes ) {
			// Utiliza todos os params, exceto pelos presentes em $atributos
			// Se o prefixo foi dado, extrai somente chaves prefixadas, e compara-as com os $atributos já sem o prefixo
			foreach ( $this->_params as $chave => $valor ) {
				if ( is_null( $valor ) )
					continue;
				if ( $prefixo && strpos( $chave, $prefixo ) !== 0 )
					continue;
				if ( $prefixo )
					$chave = substr( $chave, strlen( $prefixo ) );
				if ( isset( $atributos[ $chave ] ) )
					continue;
				$chaveFinal = ( self::ATTR_PREFIXAR_DATA & $opcoes ? 'data-' : '' ) . $this->hifenizar( $chave );
				$arrValores[ $chaveFinal ] = $valor;
			}
		}
		
		if ( !count( $arrValores ) )
			return false;
		
		$arrValores = $this->trimStrings( $arrValores );
		
		// Gera o HTML
		
		$html = array();
		
		foreach ( $arrValores as $a => $v ) {
			if ( is_null( $v ) || is_object( $v ) ) {
				continue;
			} elseif ( is_bool( $v ) ) {
				if ( $v )
					$html[] = $a;
				continue;
			} elseif ( is_array( $v ) ) {
				// $v = implode( ' ', $v );
				$v = json_encode( $v );
			}
			$html[] = $a . '="' . htmlentities( $v, ENT_QUOTES, 'UTF-8' ) . '"';
		}
		
		return implode( ' ', $html );
		
	}
	
	public function trimStrings( $arr ) {
		// Recursivamente itera pela array, aplicando trim seletivamente às strings
		foreach ( $arr as $chave => $valor ) {
			if ( is_string( $valor ) && $valor !== '' )
				$arr[$chave] = trim( $valor );
			elseif ( is_array( $valor ) )
				$arr[$chave] = $this->trimStrings( $valor );
		}
		return $arr;
	}
	
}