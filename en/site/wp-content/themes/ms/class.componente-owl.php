<?php

class Componente_Owl extends Componente_Abstrato {
	
	protected $_attrWrapper = array( 'id', 'class', 'style' );
	
	public function __construct( $novosParams = null ) {
		// Configurações do componente
		$this->_nome = 'Owl';
		$this->_slug = 'owl';
		$this->_aceitarParamsNovos = false;
		$this->_padroes = array(
			'id'				=> null,
			'style'				=> null,
			'class'				=> 'owl-carousel',
			'slides'			=> null,
			'opcoes'			=> array(
				'items'				=> 1,
				'loop'				=> true,
				'nav'				=> true,
				'dots'				=> true,
				'autoplay'			=> true,
				'autoplayTimeout'	=> 8000,
				'autoplayHoverPause'=> true,
			),
		);
		$this->_sanitizadores = array(
		);
		$this->_obrigatorios = array(
			'slides'			=> true,
			'opcoes'			=> true,
		);
		$this->_validadores = array(
			'id'				=> 'is_string',
			'style'				=> 'is_string',
			'class'				=> 'is_string',
			'slides'			=> 'is_array',
			'opcoes'			=> 'is_array',
		);
		$this->_regrasJuncao = array(
			'class'				=> '+',
			'opcoes'			=> '+',
		);
		// Inicialização
		parent::__construct( $novosParams );
	}
	
	public function renderizar( $_imprimir = true ) {
		
		if ( !$this->_params['id'] )
			$this->_params['id'] = $this->_id;
		
		return parent::renderizar( $_imprimir );
		
	}
		
}